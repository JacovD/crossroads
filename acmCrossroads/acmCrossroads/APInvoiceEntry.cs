using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using PX.Common;
using PX.Data;
using PX.Data.DependencyInjection;
using PX.Objects.Common;
using PX.Objects.Common.Extensions;
using PX.Objects.Common.Bql;
using PX.Objects.Common.Discount;
using PX.Objects.GL;
using PX.Objects.GL.FinPeriods;
using PX.Objects.CM;
using PX.Objects.CS;
using PX.Objects.CR;
using PX.Objects.CT;
using PX.Objects.PM;
using PX.Objects.TX;
using PX.Objects.IN;
using PX.Objects.CA;
using PX.Objects.BQLConstants;
using PX.Objects.EP;
using PX.Objects.PO;
using PX.Objects.SO;
using PX.Objects.DR;
using PX.Objects.AR;
using AP1099Hist = PX.Objects.AP.Overrides.APDocumentRelease.AP1099Hist;
using AP1099Yr = PX.Objects.AP.Overrides.APDocumentRelease.AP1099Yr;
using PX.Objects.GL.Reclassification.UI;
using Branch = PX.Objects.GL.Branch;
using CRLocation = PX.Objects.CR.Standalone.Location;
using PX.Objects.AP.BQL;
using PX.Objects;
using PX.Objects.AP;

namespace PX.Objects.AP
{
    public class APInvoiceEntry_Extension : PXGraphExtension<APInvoiceEntry>
    {
        #region Event Handlers

        protected void APInvoice_CuryDocBal_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            APInvoice row = (APInvoice)e.Row;
            if (row != null)
            {
                Base.Document.SetValueExt<APInvoice.curyOrigDocAmt>(row, row.CuryDocBal);
                Base.Document.SetValueExt<APInvoice.curyTaxAmt>(row, row.CuryTaxTotal);
            }
        }

        protected void APTran_UsrFleetNumber_FieldDefaulting(PXCache cache, PXFieldDefaultingEventArgs e)
        {
            var row = (APTran)e.Row;
            if (row == null) { return; }

            APTranExt itemExt = PXCache<APTran>.GetExtension<APTranExt>(row);

            POReceiptLine Rec = PXSelect<POReceiptLine,
                                   Where<POReceiptLine.receiptNbr, Equal<Required<POReceiptLine.receiptNbr>>>>
                                        .Select(Base, row.ReceiptNbr);

            if (Rec != null)
            {
                POReceiptLineExt POReceiptLineExt = PXCache<POReceiptLine>.GetExtension<POReceiptLineExt>(Rec);

                if (itemExt != null && POReceiptLineExt != null)
                {
                    itemExt.UsrFleetNumber = POReceiptLineExt.UsrProjectVehicle;
                }
            }
        }


        #endregion
    }
}
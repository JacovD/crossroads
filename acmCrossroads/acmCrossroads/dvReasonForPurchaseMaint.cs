using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;


namespace acmCrossroads.acmCrossroads
{
    public class dvReasonForPurchaseMaint : PXGraph<dvReasonForPurchaseMaint>
    {
        public PXSelect<dvReasonForPurchase> ReasonForPurchases;
        public PXSave<dvReasonForPurchase> Save;
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using PX.Objects.CS;
using PX.Objects.FA;
using acmCrossroads.Lib;
using RestSharp;
using PX.Objects.AR;
using PX.Objects.AP;

namespace acmCrossroads.acmCrossroads
{
    public class dvSendAllEntry : PXGraph<dvSendAllEntry>
    {

        public PXCancel<dvCrossroadsIntegrationAudit> Cancel;
        public PXSelect<dvCrossroadsIntegrationAudit> AuditFilter;
        public PXSelect<dvCrossroadsIntegrationAudit,
            Where<dvCrossroadsIntegrationAudit.refNum, Equal<Current<dvCrossroadsIntegrationAudit.refNum>>,
                And<dvCrossroadsIntegrationAudit.returnCode, Equal<Current<dvCrossroadsIntegrationAudit.returnCode>>>>> AuditLines;

        #region Fixed Assets
        public PXAction<dvCrossroadsIntegrationAudit> SendVehicles;
        [PXButton]
        [PXUIField(DisplayName = "Send Vehicles")]
        public virtual IEnumerable sendVehicles(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                PXResultset<FixedAsset> items = PXSelect<FixedAsset>.Select(this);
                if (items != null)
                {
                    foreach (FixedAsset row in items)
                    {
                        if (!row.ParentAssetID.HasValue && !row.SplittedFrom.HasValue)
                        {
                            bool isVehicle = false;
                            FADetails fADetails = PXSelect<FADetails, Where<FADetails.assetID, Equal<Required<FADetails.assetID>>>>.SelectSingleBound(this, null, row.AssetID);
                            if (fADetails != null)
                            {
                                FADetailsExt detailsExt = fADetails.GetExtension<FADetailsExt>();
                                if (detailsExt != null)
                                {
                                    if (!string.IsNullOrEmpty(detailsExt.UsrAssetGroup))
                                    {
                                        switch (detailsExt.UsrAssetGroup)
                                        {
                                            case "B":
                                                {
                                                    isVehicle = true;
                                                    break;
                                                }
                                            case "C":
                                                {
                                                    isVehicle = true;
                                                    break;
                                                }
                                            case "D":
                                                {
                                                    isVehicle = true;
                                                    break;
                                                }
                                            case "E":
                                                {
                                                    isVehicle = true;
                                                    break;
                                                }
                                            case "J":
                                                {
                                                    isVehicle = true;
                                                    break;
                                                }
                                            case "K":
                                                {
                                                    isVehicle = true;
                                                    break;
                                                }
                                            case "L":
                                                {
                                                    isVehicle = true;
                                                    break;
                                                }
                                            default:
                                                {
                                                    isVehicle = false;
                                                    break;
                                                }
                                        }

                                        if (isVehicle)
                                        {
                                            AssetMaint graph = CreateInstance<AssetMaint>();
                                            graph.Clear();
                                            graph.Asset.Current = row;
                                            graph.Asset.Current.Description = graph.Asset.Current.Description + " ";
                                            graph.Asset.Update(graph.Asset.Current);
                                            graph.Actions.PressSave();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
            return adapter.Get();
        }
        #endregion

        #region Customers
        public PXAction<dvCrossroadsIntegrationAudit> SendCustomers;
        [PXButton]
        [PXUIField(DisplayName = "Send Customers")]
        public virtual IEnumerable sendCustomers(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                PXResultset<Customer> items = PXSelect<Customer>.Select(this);
                if (items != null)
                {
                    foreach (Customer row in items)
                    {
                        CustomerMaint graph = CreateInstance<CustomerMaint>();
                        graph.Clear();
                        graph.BAccount.Current = row;
                        graph.BAccount.Current.AcctName = graph.BAccount.Current.AcctName + " ";
                        graph.BAccount.Update(graph.BAccount.Current);
                        graph.Actions.PressSave();
                    }
                }
            });
            return adapter.Get();
        }
        #endregion

        #region Suppliers
        public PXAction<dvCrossroadsIntegrationAudit> SendSuppliers;
        [PXButton]
        [PXUIField(DisplayName = "Send Suppliers")]
        public virtual IEnumerable sendSuppliers(PXAdapter adapter)
        {
            PXLongOperation.StartOperation(this, delegate ()
            {
                PXResultset<VendorR> items = PXSelect<VendorR>.Select(this);
                if (items != null)
                {
                    foreach (VendorR row in items)
                    {
                        VendorMaint graph = CreateInstance<VendorMaint>();
                        graph.Clear();
                        graph.BAccount.Current = row;
                        graph.BAccount.Current.AcctName = graph.BAccount.Current.AcctName + " ";
                        graph.BAccount.Update(graph.BAccount.Current);
                        graph.Actions.PressSave();
                    }
                }
            });
            return adapter.Get();
        }
        #endregion

    }
}
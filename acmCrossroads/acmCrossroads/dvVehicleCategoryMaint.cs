using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using acmCrossroads.Lib;
using RestSharp;


namespace acmCrossroads.acmCrossroads
{
    public class dvVehicleCategoryMaint : PXGraph<dvVehicleCategoryMaint>
    {
        public PXSelect<dvVehicleCategory> VehicleCategory;
        public PXSave<dvVehicleCategory> Save;
        public PXCancel<dvVehicleCategory> Cancel;

        #region Event Handlers

        //protected void VehicleCategory_RowPersisting(PXCache cache, PXRowPersistingEventArgs e)
        //{
        //    dvVehicleCategory row = (dvVehicleCategory)e.Row;
        //    string token = apiAuthentication.getToken(this);
        //    if (!string.IsNullOrEmpty(token))
        //    {
        //        int companyID = PX.Data.Update.PXInstanceHelper.CurrentCompany;
        //        IRestResponse response = apiVehicleCategory.postVehicleCategory(token, this, row, companyID);
        //        if (!response.IsSuccessful)
        //        {
        //            throw new PXException("Vehicle Category Push Failed.");
        //        }
        //    }
        //}
        #endregion
    }
}
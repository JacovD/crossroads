using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.PO;

namespace PX.Objects.IN
{
    //public class INReceiptEntry : PXGraph<INReceiptEntry>
    public class INReceiptEntry_Extension : PXGraphExtension<INReceiptEntry>
    {
       
        protected void INTran_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var INTranrow = (INTran)e.Row;
            if (INTranrow == null) { return; }

            INTranExt itemExt = PXCache<INTran>.GetExtension<INTranExt>(INTranrow);

            POReceipt PoRept = PXSelect<POReceipt,
                                  Where<POReceipt.invtRefNbr, Equal<Required<POReceipt.invtRefNbr>>>>
                                       .Select(Base, INTranrow.RefNbr);

            if (PoRept == null) { return; }

            POReceiptLine Rec = PXSelect<POReceiptLine,
                                    Where<POReceiptLine.receiptNbr, Equal<Required<POReceiptLine.receiptNbr>>>>
                                         .Select(Base, PoRept.ReceiptNbr);

            if (Rec == null) { return; }

            if (Rec != null)
            {
                POReceiptLineExt POReceiptLineExt = PXCache<POReceiptLine>.GetExtension<POReceiptLineExt>(Rec);
                // INTranExt itemExt = PXCache<INTran>.GetExtension<INTranExt>(INTranrow);

                // throw new PXException("eet bugs", POReceiptLineExt.UsrProjectVehicle.ToString());
                if (itemExt != null && POReceiptLineExt != null)
                {
                    itemExt.UsrFleetNumber = POReceiptLineExt.UsrProjectVehicle;
                }
            }


        }
    }
}

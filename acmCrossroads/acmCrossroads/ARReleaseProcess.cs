﻿using PX.Data;
using PX.Data;
using PX.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using ContextFieldDescriptor = PX.Data.PXGraph.ContextFieldDescriptor;
using PX.Objects.CS;
using PX.Objects.AR;

namespace PX.Objects.GL
{
    public class ARReleaseProcessExt : PXGraphExtension<ARReleaseProcess>
    {
        /*
        public delegate void PostBatchProcDelegate(Batch b, bool createintercompany);
        [PXOverride]
        public virtual void PostBatchProc(Batch b, bool createintercompany, PostBatchProcDelegate baseMethod)
        {
            //your code here
            baseMethod(b, createintercompany);
            //or here
        }*/

        public delegate void SaveBatchForDocumentDel(JournalEntry je, ARRegister doc);

        [PXOverride]
        public virtual void SaveBatchForDocument(JournalEntry je, ARRegister doc, SaveBatchForDocumentDel del)
        {
         /*   foreach (GLTran item in je.GLTranModuleBatNbr.Select())
            {
                ARTran aRTran = PXSelect<ARTran,
                        Where<ARTran.tranType, Equal<Required<ARTran.tranType>>,
                            And<ARTran.refNbr, Equal<Required<ARTran.refNbr>>,
                                And<ARTran.inventoryID, Equal<Required<ARTran.inventoryID>>>>>>
                    .Select(je, doc.DocType, doc.RefNbr, item.InventoryID);

                ARTranExt aRTranExt = PXCache<ARTran>.GetExtension<ARTranExt>(aRTran);
                GLTranExt itemExt = PXCache<GLTran>.GetExtension<GLTranExt>(item);
                if (itemExt == null || aRTranExt == null) continue;
                itemExt.UsrFleetNumber = aRTranExt.UsrFleetNumber;
            }
            */
            if (del != null)
                del.Invoke(je, doc);
          


        }

    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using acmCrossroads.Lib;
using RestSharp;
using PX.Objects.PO;

namespace acmCrossroads.acmCrossroads
{
    public class dvVehicleMakeMaint : PXGraph<dvVehicleMakeMaint>
    {
        public PXSelect<dvVehicleMake> VehicleMake;
        public PXSave<dvVehicleMake> Save;
        public PXCancel<dvVehicleMake> Cancel;

        #region Event Handlers

        //protected void VehicleMake_RowPersisting(PXCache cache, PXRowPersistingEventArgs e)
        //{
        //    dvVehicleMake row = (dvVehicleMake)e.Row;
        //    string token = apiAuthentication.getToken(this);
        //    if (!string.IsNullOrEmpty(token))
        //    {
        //        int companyID = PX.Data.Update.PXInstanceHelper.CurrentCompany;
        //        IRestResponse response = apiVehicleMake.postVehicleMake(token, this, row, companyID);
        //        if (!response.IsSuccessful)
        //        {
        //            throw new PXException("Vehicle Make Push Failed.");
        //        }
        //    }
        //}
        #endregion

    }
}
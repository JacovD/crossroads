using acmCrossroads.acmCrossroads;
using acmCrossroads.Lib;
using PX.Data;
using PX.Objects.AR;
using PX.Objects.CR;
using RestSharp;
using System.Collections;

namespace PX.Objects.PO
{
    
    public class POReceiptEntry_Extension : PXGraphExtension<POReceiptEntry>
    {

        #region Event Handlers

        //protected void POReceipt_InvoiceNbr_FieldVerifying(PXCache cache, PXFieldVerifyingEventArgs e)
        //{
        //    POReceipt row = (POReceipt)e.Row;
        //    if (row != null)
        //    {
        //        POReceipt batch = PXSelect<POReceipt, Where<POReceipt.invoiceNbr, Equal<Required<POReceipt.invoiceNbr>>, And<POReceipt.receiptType, Equal<POReceiptType.poreceipt>>>>.SelectSingleBound(Base, null, e.NewValue.ToString());
        //        if (batch != null)
        //        {
        //            if (!string.IsNullOrEmpty(batch.InvoiceNbr))
        //            {
        //                if (batch.InvoiceNbr.ToUpper() == e.NewValue.ToString().ToUpper())
        //                {
        //                    e.Cancel = true;
        //                    throw new PXException("Duplicate Vendor PO Reference(" + e.NewValue.ToString() + ").");
        //                }
        //            }
        //        }
        //    }
        //}

        protected void POReceipt_UsrVendorGRN_FieldVerifying(PXCache cache, PXFieldVerifyingEventArgs e)
        {
            POReceipt row = (POReceipt)e.Row;
            if (row != null)
            {
                POReceipt batch = PXSelect<POReceipt, Where<POReceiptExt.usrVendorGRN, Equal<Required<POReceiptExt.usrVendorGRN>>, And<POReceipt.invoiceNbr, Equal<Required<POReceipt.invoiceNbr>>, And<POReceipt.receiptType, Equal<POReceiptType.poreceipt>>>>>.SelectSingleBound(Base, null, e.NewValue.ToString(), row.InvoiceNbr);
                if (batch != null)
                {
                    if (!string.IsNullOrEmpty(batch.GetExtension<POReceiptExt>().UsrVendorGRN))
                    {
                        if (batch.GetExtension<POReceiptExt>().UsrVendorGRN.ToUpper() == e.NewValue.ToString().ToUpper())
                        {
                            e.Cancel = true;
                            throw new PXException("Duplicate Vendor PO Receipt Reference(" + e.NewValue.ToString() + ").");
                        }
                    }
                }
            }
        }

        protected void POReceiptLine_UsrProjectVehicle_FieldVerifying(PXCache cache, PXFieldVerifyingEventArgs e)
        {
            POReceiptLine row = (POReceiptLine) e.Row;
            if (row != null)
            {
            }
        }

        protected void POReceiptLine_InventoryID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e, PXFieldUpdated InvokeBaseHandler)
        {
            if (InvokeBaseHandler != null)
                InvokeBaseHandler(cache, e);

            var row = (POReceiptLine)e.Row;
            if (row == null) return;
            POReceiptLineExt pOReceiptLineExt = PXCache<POReceiptLine>.GetExtension<POReceiptLineExt>(row);
            if (pOReceiptLineExt == null) return;

            

            
        }

        #endregion
    }
}
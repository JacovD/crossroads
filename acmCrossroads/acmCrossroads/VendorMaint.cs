using System;
using System.Collections.Generic;
using System.Collections;
using System.Globalization;
using System.Linq;
using PX.Data;
using PX.Objects.CS;
using PX.Objects.CA;
using PX.Objects.CR;
using PX.Objects.CM;
using PX.Objects.Common;
using PX.Objects.GL;
using PX.Objects.PO;
using PX.Objects.TX;
using PX.SM;
using Branch = PX.SM.Branch;
using PX.Objects;
using PX.Objects.AP;
using acmCrossroads.Lib;
using acmCrossroads;
using RestSharp;

namespace PX.Objects.AP
{
    public class VendorMaint_Extension : PXGraphExtension<VendorMaint>
    {
        #region Event Handlers
        //protected void VendorR_RowPersisted(PXCache cache, PXRowPersistedEventArgs e)
        protected void VendorR_RowPersisted(PXCache cache, PXRowPersistedEventArgs e)
        {
            var row = (VendorR)e.Row;
            if (Base.BAccount.Cache.GetStatus(row) == PXEntryStatus.Deleted) return;
            if (row.BAccountID <= 0) return;
            if (row.DefAddressID <= 0) return;
            if (row.DefContactID <= 0) return;
            if (row.DefLocationID <= 0) return;
            if (row.BAccountID > 0)
            {

                Location locationDetail = PXSelect<Location, Where<Location.locationID, Equal<Required<Location.locationID>>>>.SelectSingleBound(Base, null, row.DefLocationID);
                if(locationDetail == null)
                {
                    throw new PXException("Vendor Push Failed - Def Location Null.");
                }

                string token = apiAuthentication.getToken(Base);
                if (!string.IsNullOrEmpty(token))
                {
                    int companyID = PX.Data.Update.PXInstanceHelper.CurrentCompany;
                    IRestResponse response = apiVendor.postVendor(token, Base, row, companyID);
                    if (!RestSharpExtensionMethods.IsSuccessful(response))
                    {
                        throw new PXException("Vendor Push Failed.");
                    }
                    else
                    {
                        abitGenix.setLastSyncDate(Base, "Vendor", row.BAccountID.ToString());
                        Base.BAccount.Cache.RaiseFieldUpdated<BAccountExt.usrVendorLastSyncDate>(row, row.GetExtension<BAccountExt>().UsrVendorLastSyncDate);
                    }
                }
            }
        }

        protected void Vendor_UsrVendorLastSyncDate_FieldUpdating(PXCache cache, PXFieldUpdatingEventArgs e)
        {
            Vendor row = (Vendor)e.Row;
            if (row != null)
            {
                if (row.BAccountID.HasValue)
                {
                    string d1 = row.GetExtension<BAccountExt>().UsrVendorLastSyncDate ?? "";
                    string d2 = abitGenix.getLastSyncDate(Base, "Vendor", row.BAccountID.ToString()) ?? "";
                    if (d1 != d2)
                    {
                        Base.BAccount.Cache.SetValue<BAccountExt.usrVendorLastSyncDate>(row, abitGenix.getLastSyncDate(Base, "Vendor", row.BAccountID.ToString()));
                    }
                }
            }
        }


        protected void Vendor_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            Vendor row = (Vendor)e.Row;
            if (row != null)
            {
                string syncDate = abitGenix.getLastSyncDate(Base, "Vendor", row.BAccountID.ToString());
                Base.BAccount.Cache.SetValueExt<BAccountExt.usrVendorLastSyncDate>(row, syncDate);
            }
        }

        #endregion
    }

    public class syncTypeVendor : Constant<string>
    {
        public syncTypeVendor()
            : base("Vendor")
        {
        }
    }
}
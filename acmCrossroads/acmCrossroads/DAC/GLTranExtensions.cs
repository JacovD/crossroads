using PX.Common;
using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.GL.Attributes;
using PX.Objects.GL.DAC.Abstract;
using PX.Objects.GL.FinPeriods.TableDefinition;
using PX.Objects.GL.FinPeriods;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects.PM;
using PX.Objects.TX;
using PX.Objects;
using System.Collections.Generic;
using System;

namespace PX.Objects.GL
{
    public class GLTranExt : PXCacheExtension<PX.Objects.GL.GLTran>
    {
        #region UsrFleetNumber
        [PXDBString(255)]
        [PXUIField(DisplayName = "Fleet Number")]

        public virtual string UsrFleetNumber { get; set; }
        public abstract class usrFleetNumber : IBqlField { }
        #endregion

        
    }

    [PXNonInstantiatedExtension]
    public class GL_GLTran_ExistingColumn : PXCacheExtension<PX.Objects.GL.GLTran>
    {
        #region RefNbr  
        [PXDBString(255, IsUnicode = true)]
        [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
        [PXDBLiteDefault(typeof(Batch.refNbr), DefaultForUpdate = false, DefaultForInsert = false)]
        [PXUIField(DisplayName = "Ref. Number", Visibility = PXUIVisibility.Visible)]
        public string RefNbr { get; set; }
        #endregion
    }

}
﻿using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.CM;
using PX.Objects.CS;
using PX.Objects.FA;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects.PO;
using PX.Objects;
using System.Collections.Generic;
using System;
using acmCrossroads.acmCrossroads;

namespace PX.Objects.FA
{
    [PXNonInstantiatedExtension]
    public class FA_FADetails_ExistingColumn : PXCacheExtension<PX.Objects.FA.FADetails>
    {
        #region Manufacturer  
        [PXMergeAttributes(Method = MergeMethod.Append)]

        public string Manufacturer { get; set; }
        #endregion

        #region LeaseNumber  
        [PXDBString(20, IsUnicode = true)]
        [PXUIField(DisplayName = "Lease Number")]
        public string LeaseNumber { get; set; }
        #endregion
    }   


    public class FADetailsExt : PXCacheExtension<PX.Objects.FA.FADetails>
    {
        #region UsrRegistrationNbr
        [PXDBString(12)]
        //[PXDefault(PersistingCheck = PXPersistingCheck.NullOrBlank)]
        [PXUIField(DisplayName = "Registration Number")]

        public virtual string UsrRegistrationNbr { get; set; }
        public abstract class usrRegistrationNbr : IBqlField { }
        #endregion

        #region UsrFleetNbr
        [PXDBString(50)]
        //[PXDefault(PersistingCheck = PXPersistingCheck.NullOrBlank)]
        [PXUIField(DisplayName = "Fleet Number")]

        public virtual string UsrFleetNbr { get; set; }
        public abstract class usrFleetNbr : IBqlField { }
        #endregion

        #region UsrAssetGroup
        [PXDBString(10)]
        //[PXDefault(PersistingCheck = PXPersistingCheck.NullOrBlank)]
        [PXUIField(DisplayName = "Asset Group")]

        public virtual string UsrAssetGroup { get; set; }
        public abstract class usrAssetGroup : IBqlField { }
        #endregion

        #region UsrAssetNbr2
        [PXDBString(10)]
        [PXUIField(DisplayName = "Licence Number")]

        public virtual string UsrAssetNbr2 { get; set; }
        public abstract class usrAssetNbr2 : IBqlField { }
        #endregion

        #region UsrIssueDate
        [PXDBDate]
        [PXUIField(DisplayName = "IssueDate")]

        public virtual DateTime? UsrIssueDate { get; set; }
        public abstract class usrIssueDate : IBqlField { }
        #endregion

        #region UsrBuildDate
        [PXDBDate]
        [PXUIField(DisplayName = "Build Date")]

        public virtual DateTime? UsrBuildDate { get; set; }
        public abstract class usrBuildDate : IBqlField { }
        #endregion

        #region UsrFleetDate
        [PXDBDate]
        [PXUIField(DisplayName = "Fleet Date")]
        //[PXDefault(PersistingCheck = PXPersistingCheck.NullOrBlank)]

        public virtual DateTime? UsrFleetDate { get; set; }
        public abstract class usrFleetDate : IBqlField { }
        #endregion

        #region UsrConfiguration
        [PXDBString(50)]
        [PXUIField(DisplayName = "Configuration")]

        public virtual string UsrConfiguration { get; set; }
        public abstract class usrConfiguration : IBqlField { }
        #endregion

        #region UsrVINNbr
        [PXDBString(30)]
        //[PXDefault(PersistingCheck = PXPersistingCheck.NullOrBlank)]
        [PXUIField(DisplayName = "VIN Number")]

        public virtual string UsrVINNbr { get; set; }
        public abstract class usrVINNbr : IBqlField { }
        #endregion

        #region UsrAuxiliaryEquipment
        [PXDBString(50)]
        [PXUIField(DisplayName = "Auxiliary Equipment")]

        public virtual string UsrAuxiliaryEquipment { get; set; }
        public abstract class usrAuxiliaryEquipment : IBqlField { }
        #endregion

        #region UsrAuxiliaryEquipmentValue
        [PXDBDecimal]
        [PXUIField(DisplayName = "Auxiliary Equipment Value")]

        public virtual Decimal? UsrAuxiliaryEquipmentValue { get; set; }
        public abstract class usrAuxiliaryEquipmentValue : IBqlField { }
        #endregion

        #region UsrInsurance
        [PXDBString(50)]
        [PXUIField(DisplayName = "Insurance")]
        [PXStringList(new string[] { "00",
                                    "01",
                                    "02",
                                    "03",
                                    "04",
                                    "05",
                                    "06",
                                    "07",
                                    "08",
                                    "09",
                                    "10",
                                    "11",
                                    "12",
                                    "13",
                                    "16",
                                    "1",
                                    "2" }, 
                        new string[] { "00-Disposed",
                                    "01-Truck Tractor",
                                    "02-Tanker Semi-trlr",
                                    "03-General Semi-trlr",
                                    "04-Drawbar Trailer",
                                    "05-Dolly",
                                    "06-Rigid Or Pantech",
                                    "07-Bus",
                                    "08-Forklift Truck",
                                    "09-Front-end Loader",
                                    "10-PUP Tandem & Misc",
                                    "11 -M/car >R25000",
                                    "12-Sub Contractors",
                                    "13-LDV",
                                    "16-Hired Vehicles",
                                    "1-Drive Cam & Tracking",
                                    "2-Other" })]
        public virtual string UsrInsurance { get; set; }
        public abstract class usrInsurance : IBqlField { }
        #endregion

        #region UsrResidualValue
        [PXDBDecimal]
        [PXUIField(DisplayName = "Residual Value")]

        public virtual Decimal? UsrResidualValue { get; set; }
        public abstract class usrResidualValue : IBqlField { }
        #endregion

        #region UsrMarketValue
        [PXDBDecimal]
        [PXUIField(DisplayName = "Market Value")]

        public virtual Decimal? UsrMarketValue { get; set; }
        public abstract class usrMarketValue : IBqlField { }
        #endregion

        #region UsrPQMarketValue
        [PXDBDecimal]
        [PXUIField(DisplayName = "PQ Market Value")]

        public virtual Decimal? UsrPQMarketValue { get; set; }
        public abstract class usrPQMarketValue : IBqlField { }
        #endregion

        #region UsrYearModel
        [PXDBString(4)]
        //[PXDefault(PersistingCheck = PXPersistingCheck.NullOrBlank)]
        [PXUIField(DisplayName = "Year Model")]

        public virtual string UsrYearModel { get; set; }
        public abstract class usrYearModel : IBqlField { }
        #endregion

        #region UsrEngineNbr
        [PXDBString(20)]
        [PXUIField(DisplayName = "Engine Number")]

        public virtual string UsrEngineNbr { get; set; }
        public abstract class usrEngineNbr : IBqlField { }
        #endregion

        #region UsrITNbr
        [PXDBString(20)]
        [PXUIField(DisplayName = "IT Number")]

        public virtual string UsrITNbr { get; set; }
        public abstract class usrITNbr : IBqlField { }
        #endregion

        #region UsrVehicleMake
        [PXDBString(255)]
        //[PXDefault(PersistingCheck = PXPersistingCheck.NullOrBlank)]
        [PXUIField(DisplayName = "Vehicle Make")]
        [PXSelector(typeof(Search<dvVehicleMake.vehicleMakeCode>),
            DescriptionField = typeof(dvVehicleMake.vehicleMakeDescription),
            SubstituteKey = typeof(dvVehicleMake.vehicleMakeCode))]
        public virtual string UsrVehicleMake { get; set; }
        public abstract class usrVehicleMake : IBqlField { }
        #endregion

        #region UsrVehicleCategory
        [PXDBString(255)]
        //[PXDefault(PersistingCheck = PXPersistingCheck.NullOrBlank)]
        [PXUIField(DisplayName = "Vehicle Category")]
        [PXSelector(typeof(Search<dvVehicleCategory.vehicleCategory>),
            SubstituteKey = typeof(dvVehicleCategory.vehicleCategory))]
        public virtual string UsrVehicleCategory { get; set; }
        public abstract class usrVehicleCategory : IBqlField { }
        #endregion

        #region UsrVehicleModel
        [PXDBString(255)]
        //[PXDefault(PersistingCheck = PXPersistingCheck.NullOrBlank)]
        [PXUIField(DisplayName = "Vehicle Model")]
        [PXSelector(typeof(Search<dvVehicleModel.vehicleModelID, Where<dvVehicleModel.vehicleMake, Equal<Current<usrVehicleMake>>, And<dvVehicleModel.vehicleCategory, Equal<Current<usrVehicleCategory>>>>>),
            DescriptionField = typeof(dvVehicleModel.modelDescription),
            SubstituteKey = typeof(dvVehicleModel.modelCode))]
        public virtual string UsrVehicleModel { get; set; }
        public abstract class usrVehicleModel : IBqlField { }
        #endregion

        #region UsrTankCapacity
        [PXDBInt]
        [PXUIField(DisplayName = "Tank Capacity")]

        public virtual int? UsrTankCapacity { get; set; }
        public abstract class usrTankCapacity : IBqlField { }
        #endregion
    }
}
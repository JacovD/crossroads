using PX.Common;
using PX.Data.EP;
using PX.Data;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.GL.Attributes;
using PX.Objects.GL.DAC;
using PX.Objects.GL;
using PX.Objects;
using System.Collections.Generic;
using System;

namespace PX.Objects.GL
{
  public class BatchExt : PXCacheExtension<PX.Objects.GL.Batch>
  {
    #region UsrUniqueExtReference
    [PXDBString(255)]
    //[PXDefault(PersistingCheck = PXPersistingCheck.NullOrBlank)]
    [PXUIField(DisplayName="Unique Ext Ref")]
    public virtual string UsrUniqueExtReference { get; set; }
    public abstract class usrUniqueExtReference : IBqlField { }
    #endregion
  }
}
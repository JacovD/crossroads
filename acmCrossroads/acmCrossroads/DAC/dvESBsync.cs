﻿﻿namespace acmCrossroads.acmCrossroads
{
	using System;
	using PX.Data;
	
	[System.SerializableAttribute()]
	public class dvESBsync : PX.Data.IBqlTable
	{
		#region dvESBsyncID
		public abstract class DvESBsyncID : PX.Data.IBqlField
		{
		}
		protected int? _dvESBsyncID;
		[PXDBIdentity(IsKey = true)]
		[PXUIField(Enabled = false)]
		public virtual int? dvESBsyncID
		{
			get
			{
				return this._dvESBsyncID;
			}
			set
			{
				this._dvESBsyncID = value;
			}
		}
		#endregion
		#region SyncLocation
		public abstract class syncLocation : PX.Data.IBqlField
		{
		}
		protected string _SyncLocation;
		[PXDBString(255, IsUnicode = true)]
		[PXUIField(DisplayName = "SyncLocation")]
		public virtual string SyncLocation
		{
			get
			{
				return this._SyncLocation;
			}
			set
			{
				this._SyncLocation = value;
			}
		}
		#endregion
		#region RefNbr
		public abstract class refNbr : PX.Data.IBqlField
		{
		}
		protected string _RefNbr;
		[PXDBString(255, IsUnicode = true)]
		[PXUIField(DisplayName = "RefNbr")]
		public virtual string RefNbr
		{
			get
			{
				return this._RefNbr;
			}
			set
			{
				this._RefNbr = value;
			}
		}
		#endregion
		#region Json
		public abstract class json : PX.Data.IBqlField
		{
		}
		protected string _Json;
		[PXDBString(-1, IsUnicode = true)]
		[PXUIField(DisplayName = "Json")]
		public virtual string Json
		{
			get
			{
				return this._Json;
			}
			set
			{
				this._Json = value;
			}
		}
		#endregion
		#region DateSynced
		public abstract class dateSynced : PX.Data.IBqlField
		{
		}
		protected string _DateSynced;
		[PXDBString(255, IsUnicode = true)]
		[PXUIField(DisplayName = "DateSynced")]
		public virtual string DateSynced
		{
			get
			{
				return this._DateSynced;
			}
			set
			{
				this._DateSynced = value;
			}
		}
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        //public abstract class noteID : PX.Data.IBqlField
        //{
        //}
        //protected Guid? _NoteID;
        //[PXNote()]
        //public virtual Guid? NoteID
        //{
        //    get
        //    {
        //        return this._NoteID;
        //    }
        //    set
        //    {
        //        this._NoteID = value;
        //    }
        //}
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
    }
}

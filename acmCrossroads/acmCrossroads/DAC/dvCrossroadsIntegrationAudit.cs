﻿﻿namespace acmCrossroads.acmCrossroads
{
	using System;
	using PX.Data;
	
	[System.SerializableAttribute()]
	public class dvCrossroadsIntegrationAudit : PX.Data.IBqlTable
	{
		#region AuditID
		public abstract class auditID : PX.Data.IBqlField
		{
		}
		protected int? _AuditID;
		[PXDBIdentity(IsKey = true)]
		[PXUIField(Enabled = false)]
		public virtual int? AuditID
		{
			get
			{
				return this._AuditID;
			}
			set
			{
				this._AuditID = value;
			}
		}
		#endregion		
		#region RefNum
		public abstract class refNum : PX.Data.IBqlField
		{
		}
		protected string _RefNum;
		[PXDBString(255, IsUnicode = true)]
		[PXUIField(DisplayName = "RefNum")]
		public virtual string RefNum
		{
			get
			{
				return this._RefNum;
			}
			set
			{
				this._RefNum = value;
			}
		}
		#endregion
		#region ESBToken
		public abstract class eSBToken : PX.Data.IBqlField
		{
		}
		protected string _ESBToken;
		[PXDBString(-1, IsUnicode = true)]
		[PXUIField(DisplayName = "ESBToken")]
		public virtual string ESBToken
		{
			get
			{
				return this._ESBToken;
			}
			set
			{
				this._ESBToken = value;
			}
		}
		#endregion
		#region JSONObject
		public abstract class jSONObject : PX.Data.IBqlField
		{
		}
		protected string _JSONObject;
		[PXDBString(-1, IsUnicode = true)]
		[PXUIField(DisplayName = "JSONObject")]
		public virtual string JSONObject
		{
			get
			{
				return this._JSONObject;
			}
			set
			{
				this._JSONObject = value;
			}
		}
		#endregion
		#region ReturnCode
		public abstract class returnCode : PX.Data.IBqlField
		{
		}
		protected string _ReturnCode;
		[PXDBString(255, IsUnicode = true)]
		[PXUIField(DisplayName = "ReturnCode")]
		public virtual string ReturnCode
		{
			get
			{
				return this._ReturnCode;
			}
			set
			{
				this._ReturnCode = value;
			}
		}
		#endregion
		#region ReturnMessage
		public abstract class returnMessage : PX.Data.IBqlField
		{
		}
		protected string _ReturnMessage;
		[PXDBString(-1, IsUnicode = true)]
		[PXUIField(DisplayName = "ReturnMessage")]
		public virtual string ReturnMessage
		{
			get
			{
				return this._ReturnMessage;
			}
			set
			{
				this._ReturnMessage = value;
			}
		}
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        //public abstract class noteID : PX.Data.IBqlField
        //{
        //}
        //protected Guid? _NoteID;
        //[PXNote()]
        //public virtual Guid? NoteID
        //{
        //    get
        //    {
        //        return this._NoteID;
        //    }
        //    set
        //    {
        //        this._NoteID = value;
        //    }
        //}
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
    }
}

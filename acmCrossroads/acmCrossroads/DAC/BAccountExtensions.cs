using PX.Data.EP;
using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.AR;
using PX.Objects.CR.MassProcess;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.GL;
using PX.Objects.TX;
using PX.Objects;
using PX.SM;
using PX.TM;
using System.Collections.Generic;
using System.Diagnostics;
using System;
using acmCrossroads.acmCrossroads;

namespace PX.Objects.CR
{
    public class BAccountExt : PXCacheExtension<PX.Objects.CR.BAccount>
    {
        #region UsrCreditLimit
        [PXDBDecimal]
        [PXUIField(DisplayName = "Credit Limit")]

        public virtual Decimal? UsrCreditLimit { get; set; }
        public abstract class usrCreditLimit : IBqlField { }
        #endregion

        #region UsrAvailableLimit
        [PXDBDecimal]
        [PXUIField(DisplayName = "Available Limit")]

        public virtual Decimal? UsrAvailableLimit { get; set; }
        public abstract class usrAvailableLimit : IBqlField { }
        #endregion

        #region UsrCustomerLastSyncDate
        [PXDBString(255)]
        [PXUIField(DisplayName = "Last Integrated Date")]
        //[PXSelector(typeof(Search<dvESBsync.dateSynced, Where<dvESBsync.syncLocation, Equal<syncTypeCustomer>, And<dvESBsync.refNbr, Equal<Current<BAccount.bAccountID>>>>, OrderBy<Desc<dvESBsync.DvESBsyncID>>>))]
        public virtual string UsrCustomerLastSyncDate { get; set; }
        public abstract class usrCustomerLastSyncDate : IBqlField { }
        #endregion

        #region UsrVendorLastSyncDate
        [PXDBString(255)]
        [PXUIField(DisplayName = "Last Integrated Date")]
        //[PXSelector(typeof(Search<dvESBsync.dateSynced, Where<dvESBsync.syncLocation, Equal<syncTypeVendor>, And<dvESBsync.refNbr, Equal<Current<BAccount.bAccountID>>>>, OrderBy<Desc<dvESBsync.DvESBsyncID>>>))]
        public virtual string UsrVendorLastSyncDate { get; set; }
        public abstract class usrVendorLastSyncDate : IBqlField { }
        #endregion

    }
}
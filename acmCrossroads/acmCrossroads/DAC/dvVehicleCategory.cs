﻿﻿namespace acmCrossroads.acmCrossroads
{
	using System;
	using PX.Data;
	
	[System.SerializableAttribute()]
	public class dvVehicleCategory : PX.Data.IBqlTable
	{
		#region VehicleCategoryID
		public abstract class vehicleCategoryID : PX.Data.IBqlField
		{
		}
		protected int? _VehicleCategoryID;
		[PXDBIdentity()]
		[PXUIField(Enabled = false)]
		public virtual int? VehicleCategoryID
		{
			get
			{
				return this._VehicleCategoryID;
			}
			set
			{
				this._VehicleCategoryID = value;
			}
		}
		#endregion
		#region VehicleCategory
		public abstract class vehicleCategory : PX.Data.IBqlField
		{
		}
		protected string _VehicleCategory;
		[PXDBString(255, IsKey = true, IsUnicode = true)]
		[PXUIField(DisplayName = "Vehicle Category")]
		public virtual string VehicleCategory
		{
			get
			{
				return this._VehicleCategory;
			}
			set
			{
				this._VehicleCategory = value;
			}
		}
        #endregion
        #region AssetGroup
        public abstract class assetGroup : PX.Data.IBqlField
        {
        }
        protected string _AssetGroup;
        [PXDBString(255, IsUnicode = true)]
        [PXUIField(DisplayName = "GL Mapping")]
        public virtual string AssetGroup
        {
            get
            {
                return this._AssetGroup;
            }
            set
            {
                this._AssetGroup = value;
            }
        }
        #endregion
        #region VehicleCategoryCOF
        public abstract class vehicleCategoryCOF : PX.Data.IBqlField
		{
		}
		protected string _VehicleCategoryCOF;
		[PXDBString(1, IsUnicode = true)]
		[PXUIField(DisplayName = "Vehicle Category COF")]
		public virtual string VehicleCategoryCOF
		{
			get
			{
				return this._VehicleCategoryCOF;
			}
			set
			{
				this._VehicleCategoryCOF = value;
			}
		}
		#endregion
		#region Active
		public abstract class active : PX.Data.IBqlField
		{
		}
		protected bool? _Active;
		[PXDBBool()]
		[PXUIField(DisplayName = "Active")]
		public virtual bool? Active
		{
			get
			{
				return this._Active;
			}
			set
			{
				this._Active = value;
			}
		}
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        //public abstract class noteID : PX.Data.IBqlField
        //{
        //}
        //protected Guid? _NoteID;
        //[PXNote()]
        //public virtual Guid? NoteID
        //{
        //    get
        //    {
        //        return this._NoteID;
        //    }
        //    set
        //    {
        //        this._NoteID = value;
        //    }
        //}
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
    }
}

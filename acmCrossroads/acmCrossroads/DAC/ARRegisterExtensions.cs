using ARCashSale = PX.Objects.AR.Standalone.ARCashSale;
using CRLocation = PX.Objects.CR.Standalone.Location;
using IRegister = PX.Objects.CM.IRegister;
using PX.Data.EP;
using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.AR.BQL;
using PX.Objects.AR;
using PX.Objects.CM;
using PX.Objects.Common.Abstractions;
using PX.Objects.Common.MigrationMode;
using PX.Objects.Common;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.GL;
using PX.Objects;
using System.Collections.Generic;
using System.Diagnostics;
using System;

namespace PX.Objects.AR
{
  public class ARRegisterExt : PXCacheExtension<PX.Objects.AR.ARRegister>
  {
    #region UsrCosignor
    [PXDBString(255)]
    [PXUIField(DisplayName= "Consignor")]

    public virtual string UsrCosignor { get; set; }
    public abstract class usrCosignor : IBqlField { }
    #endregion

    #region UsrCosignee
    [PXDBString(255)]
    [PXUIField(DisplayName="Consignee")]

    public virtual string UsrCosignee { get; set; }
    public abstract class usrCosignee : IBqlField { }
    #endregion
  }

      [PXNonInstantiatedExtension]
  public class AR_ARRegister_ExistingColumn : PXCacheExtension<PX.Objects.AR.ARRegister>
  {
      #region DocDesc  
      
[PXCustomizeBaseAttribute(typeof(PXUIFieldAttribute), "DisplayName", "Customer Reference")]
      public string DocDesc { get; set; }
      #endregion
  }
}
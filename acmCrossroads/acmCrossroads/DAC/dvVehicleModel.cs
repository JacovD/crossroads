﻿﻿namespace acmCrossroads.acmCrossroads
{
	using System;
	using PX.Data;
	
	[System.SerializableAttribute()]
	public class dvVehicleModel : PX.Data.IBqlTable
	{
		#region VehicleModelID
		public abstract class vehicleModelID : PX.Data.IBqlField
		{
		}
		protected int? _VehicleModelID;
		[PXDBIdentity(IsKey = true)]
		[PXUIField(Enabled = false)]
		public virtual int? VehicleModelID
		{
			get
			{
				return this._VehicleModelID;
			}
			set
			{
				this._VehicleModelID = value;
			}
		}
		#endregion
		#region ModelVariant
		public abstract class modelVariant : PX.Data.IBqlField
		{
		}
		protected string _ModelVariant;
		[PXDBString(255,  IsUnicode = true)]
		[PXUIField(DisplayName = "Model Variant")]
		public virtual string ModelVariant
		{
			get
			{
				return this._ModelVariant;
			}
			set
			{
				this._ModelVariant = value;
			}
		}
		#endregion
		#region Model Code
		public abstract class modelCode : PX.Data.IBqlField
		{
		}
		protected string _ModelCode;
		[PXDBString(255,  IsUnicode = true)]
		[PXUIField(DisplayName = "Model Code")]
		public virtual string ModelCode
		{
			get
			{
				return this._ModelCode;
			}
			set
			{
				this._ModelCode = value;
			}
		}
		#endregion
		#region ModelDescription
		public abstract class modelDescription : PX.Data.IBqlField
		{
		}
		protected string _ModelDescription;
		[PXDBString(-1, IsUnicode = true)]
		[PXUIField(DisplayName = "Model Description")]
		public virtual string ModelDescription
		{
			get
			{
				return this._ModelDescription;
			}
			set
			{
				this._ModelDescription = value;
			}
		}
		#endregion
		#region VehicleCategory
		public abstract class vehicleCategory : PX.Data.IBqlField
		{
		}
		protected string _VehicleCategory;
		[PXDBString()]
		[PXUIField(DisplayName = "Vehicle Category")]
        [PXSelector(typeof(Search<dvVehicleCategory.vehicleCategory>), 
            SubstituteKey = typeof(dvVehicleCategory.vehicleCategory))]
        public virtual string VehicleCategory
		{
			get
			{
				return this._VehicleCategory;
			}
			set
			{
				this._VehicleCategory = value;
			}
		}
		#endregion
		#region VehicleMake
		public abstract class vehicleMake : PX.Data.IBqlField
		{
		}
		protected string _VehicleMake;
		[PXDBString()]
		[PXUIField(DisplayName = "Vehicle Make")]
        [PXSelector(typeof(Search<dvVehicleMake.vehicleMakeCode>),
            DescriptionField = typeof(dvVehicleMake.vehicleMakeDescription),
            SubstituteKey = typeof(dvVehicleMake.vehicleMakeCode))]
        public virtual string VehicleMake
		{
			get
			{
				return this._VehicleMake;
			}
			set
			{
				this._VehicleMake = value;
			}
		}
		#endregion
		#region ModelPrice
		public abstract class modelPrice : PX.Data.IBqlField
		{
		}
		protected decimal? _ModelPrice;
		[PXDBDecimal(11)]
		[PXUIField(DisplayName = "Model Price")]
		public virtual decimal? ModelPrice
		{
			get
			{
				return this._ModelPrice;
			}
			set
			{
				this._ModelPrice = value;
			}
		}
		#endregion
		#region ModelLicenseGroup
		public abstract class modelLicenseGroup : PX.Data.IBqlField
		{
		}
		protected string _ModelLicenseGroup;
		[PXDBString(255, IsUnicode = true)]
		[PXUIField(DisplayName = "Model License Group")]
		public virtual string ModelLicenseGroup
		{
			get
			{
				return this._ModelLicenseGroup;
			}
			set
			{
				this._ModelLicenseGroup = value;
			}
		}
		#endregion
		#region EngineCapacity
		public abstract class engineCapacity : PX.Data.IBqlField
		{
		}
		protected decimal? _EngineCapacity;
		[PXDBDecimal(11)]
		[PXUIField(DisplayName = "Engine Capacity")]
		public virtual decimal? EngineCapacity
		{
			get
			{
				return this._EngineCapacity;
			}
			set
			{
				this._EngineCapacity = value;
			}
		}
		#endregion
		#region FuelTankCapacity
		public abstract class fuelTankCapacity : PX.Data.IBqlField
		{
		}
		protected decimal? _FuelTankCapacity;
		[PXDBDecimal(11)]
		[PXUIField(DisplayName = "Fuel Tank Capacity")]
		public virtual decimal? FuelTankCapacity
		{
			get
			{
				return this._FuelTankCapacity;
			}
			set
			{
				this._FuelTankCapacity = value;
			}
		}
		#endregion
		#region UnladenWeight
		public abstract class unladenWeight : PX.Data.IBqlField
		{
		}
		protected decimal? _UnladenWeight;
		[PXDBDecimal(11)]
		[PXUIField(DisplayName = "Unladen Weight")]
		public virtual decimal? UnladenWeight
		{
			get
			{
				return this._UnladenWeight;
			}
			set
			{
				this._UnladenWeight = value;
			}
		}
		#endregion
		#region ModelTareWeight
		public abstract class modelTareWeight : PX.Data.IBqlField
		{
		}
		protected decimal? _ModelTareWeight;
		[PXDBDecimal(11)]
		[PXUIField(DisplayName = "Model Tare Weight")]
		public virtual decimal? ModelTareWeight
		{
			get
			{
				return this._ModelTareWeight;
			}
			set
			{
				this._ModelTareWeight = value;
			}
		}
		#endregion
		#region ModelMaximumRevs
		public abstract class modelMaximumRevs : PX.Data.IBqlField
		{
		}
		protected decimal? _ModelMaximumRevs;
		[PXDBDecimal(11)]
		[PXUIField(DisplayName = "Model Maximum Revs")]
		public virtual decimal? ModelMaximumRevs
		{
			get
			{
				return this._ModelMaximumRevs;
			}
			set
			{
				this._ModelMaximumRevs = value;
			}
		}
		#endregion
		#region ModelFuelConsumption
		public abstract class modelFuelConsumption : PX.Data.IBqlField
		{
		}
		protected decimal? _ModelFuelConsumption;
		[PXDBDecimal(11)]
		[PXUIField(DisplayName = "Model Fuel Consumption")]
		public virtual decimal? ModelFuelConsumption
		{
			get
			{
				return this._ModelFuelConsumption;
			}
			set
			{
				this._ModelFuelConsumption = value;
			}
		}
		#endregion
		#region ModelFuelType
		public abstract class modelFuelType : PX.Data.IBqlField
		{
		}
		protected string _ModelFuelType;
		[PXDBString(255, IsUnicode = true)]
		[PXUIField(DisplayName = "Model Fuel Type")]
		public virtual string ModelFuelType
		{
			get
			{
				return this._ModelFuelType;
			}
			set
			{
				this._ModelFuelType = value;
			}
		}
		#endregion
		#region CO2Emissions
		public abstract class cO2Emissions : PX.Data.IBqlField
		{
		}
		protected decimal? _CO2Emissions;
		[PXDBDecimal(9)]
		[PXUIField(DisplayName = "CO2 Emissions")]
		public virtual decimal? CO2Emissions
		{
			get
			{
				return this._CO2Emissions;
			}
			set
			{
				this._CO2Emissions = value;
			}
		}
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        //public abstract class noteID : PX.Data.IBqlField
        //{
        //}
        //protected Guid? _NoteID;
        //[PXNote()]
        //public virtual Guid? NoteID
        //{
        //    get
        //    {
        //        return this._NoteID;
        //    }
        //    set
        //    {
        //        this._NoteID = value;
        //    }
        //}
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
    }
}

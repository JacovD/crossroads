﻿﻿namespace acmCrossroads.acmCrossroads
{
	using System;
	using PX.Data;
	
	[System.SerializableAttribute()]
	public class dvVehicleMake : PX.Data.IBqlTable
	{
		#region VehicleMakeID
		public abstract class vehicleMakeID : PX.Data.IBqlField
		{
		}
		protected int? _VehicleMakeID;
		[PXDBIdentity()]
		[PXUIField(Enabled = false)]
		public virtual int? VehicleMakeID
		{
			get
			{
				return this._VehicleMakeID;
			}
			set
			{
				this._VehicleMakeID = value;
			}
		}
		#endregion
		#region VehicleMakeCode
		public abstract class vehicleMakeCode : PX.Data.IBqlField
		{
		}
		protected string _VehicleMakeCode;
		[PXDBString(255, IsKey = true, IsUnicode = true)]
		[PXUIField(DisplayName = "Vehicle Make Code")]
		public virtual string VehicleMakeCode
		{
			get
			{
				return this._VehicleMakeCode;
			}
			set
			{
				this._VehicleMakeCode = value;
			}
		}
		#endregion
		#region VehicleMakeDescription
		public abstract class vehicleMakeDescription : PX.Data.IBqlField
		{
		}
		protected string _VehicleMakeDescription;
		[PXDBString(255, IsUnicode = true)]
		[PXUIField(DisplayName = "Vehicle Make Description")]
		public virtual string VehicleMakeDescription
		{
			get
			{
				return this._VehicleMakeDescription;
			}
			set
			{
				this._VehicleMakeDescription = value;
			}
		}
		#endregion
		#region Active
		public abstract class active : PX.Data.IBqlField
		{
		}
		protected bool? _Active;
		[PXDBBool()]
		[PXUIField(DisplayName = "Active")]
		public virtual bool? Active
		{
			get
			{
				return this._Active;
			}
			set
			{
				this._Active = value;
			}
		}
        #endregion
        #region CreatedByID
        public abstract class createdByID : PX.Data.IBqlField
        {
        }
        protected Guid? _CreatedByID;
        [PXDBCreatedByID]
        public virtual Guid? CreatedByID
        {
            get
            {
                return this._CreatedByID;
            }
            set
            {
                this._CreatedByID = value;
            }
        }
        #endregion
        #region CreatedByScreenID
        public abstract class createdByScreenID : PX.Data.IBqlField
        {
        }
        protected string _CreatedByScreenID;
        [PXDBCreatedByScreenID]
        public virtual string CreatedByScreenID
        {
            get
            {
                return this._CreatedByScreenID;
            }
            set
            {
                this._CreatedByScreenID = value;
            }
        }
        #endregion
        #region CreatedDateTime
        public abstract class createdDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _CreatedDateTime;
        [PXDBCreatedDateTime]
        public virtual DateTime? CreatedDateTime
        {
            get
            {
                return this._CreatedDateTime;
            }
            set
            {
                this._CreatedDateTime = value;
            }
        }
        #endregion
        #region LastModifiedByID
        public abstract class lastModifiedByID : PX.Data.IBqlField
        {
        }
        protected Guid? _LastModifiedByID;
        [PXDBLastModifiedByID]
        public virtual Guid? LastModifiedByID
        {
            get
            {
                return this._LastModifiedByID;
            }
            set
            {
                this._LastModifiedByID = value;
            }
        }
        #endregion
        #region LastModifiedByScreenID
        public abstract class lastModifiedByScreenID : PX.Data.IBqlField
        {
        }
        protected string _LastModifiedByScreenID;
        [PXDBLastModifiedByScreenID]
        public virtual string LastModifiedByScreenID
        {
            get
            {
                return this._LastModifiedByScreenID;
            }
            set
            {
                this._LastModifiedByScreenID = value;
            }
        }
        #endregion
        #region LastModifiedDateTime
        public abstract class lastModifiedDateTime : PX.Data.IBqlField
        {
        }
        protected DateTime? _LastModifiedDateTime;
        [PXDBLastModifiedDateTime]
        public virtual DateTime? LastModifiedDateTime
        {
            get
            {
                return this._LastModifiedDateTime;
            }
            set
            {
                this._LastModifiedDateTime = value;
            }
        }
        #endregion
        #region NoteID
        //public abstract class noteID : PX.Data.IBqlField
        //{
        //}
        //protected Guid? _NoteID;
        //[PXNote()]
        //public virtual Guid? NoteID
        //{
        //    get
        //    {
        //        return this._NoteID;
        //    }
        //    set
        //    {
        //        this._NoteID = value;
        //    }
        //}
        #endregion
        #region tstamp
        public abstract class Tstamp : PX.Data.IBqlField
        {
        }
        protected byte[] _tstamp;
        [PXDBTimestamp()]
        public virtual byte[] tstamp
        {
            get
            {
                return this._tstamp;
            }
            set
            {
                this._tstamp = value;
            }
        }
        #endregion
    }
}

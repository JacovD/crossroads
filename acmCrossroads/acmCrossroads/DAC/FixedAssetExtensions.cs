using PX.Data.EP;
using PX.Data;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.FA;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects;
using System.Collections.Generic;
using System;
using acmCrossroads.acmCrossroads;
using PX.Objects.CT;

namespace PX.Objects.FA
{

    public class FixedAssetExt : PXCacheExtension<PX.Objects.FA.FixedAsset>
    {
       

        #region UsrAssetProject
        [PXDBString(255)]
        [PXUIField(DisplayName = "Project", Enabled = false)]
        //[PXSelector(typeof(Search<Contract.contractCD, Where<Contract.nonProject , Equal<boolFalse>>>),
        //    DescriptionField = typeof(Contract.description),
        //    SubstituteKey = typeof(Contract.contractCD))]
        public virtual string UsrAssetProject { get; set; }
        public abstract class usrAssetProject : IBqlField { }
        #endregion

        #region UsrLastSyncDate
        [PXDBString(255)]
        [PXUIField(DisplayName = "Last Integrated Date")]
        //[PXSelector(typeof(Search<dvESBsync.dateSynced, Where<dvESBsync.syncLocation, Equal<syncType>, And<dvESBsync.refNbr, Equal<Current<FixedAsset.assetID>>>>, OrderBy<Desc<dvESBsync.DvESBsyncID>>>),)]
        public virtual string UsrLastSyncDate { get; set; }
        public abstract class usrLastSyncDate : IBqlField { }
        #endregion
    }

    public class syncType : Constant<string>
    {
        public syncType()
            : base("FixedAsset")
        {
        }
    }    
}
﻿using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.CM;
using PX.Objects.Common.Discount.Attributes;
using PX.Objects.Common.Discount;
using PX.Objects.Common;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.DR;  
using PX.Objects.EP;
using PX.Objects.FA;
using PX.Objects.GL.DAC.Abstract;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects.PM;
using PX.Objects.PO;
using PX.Objects.TX;
using PX.Objects;
using System.Collections.Generic;
using System;

namespace PX.Objects.AP
{
    public class INTranExt : PXCacheExtension<PX.Objects.IN.INTran>
    {
        #region UsrFleetNumber
        [PXDBString(255)]
        [PXUIField(DisplayName = "Fleet Number")]

        public virtual string UsrFleetNumber { get; set; }
        public abstract class usrFleetNumber : IBqlField { }
        
        #endregion
    }
}
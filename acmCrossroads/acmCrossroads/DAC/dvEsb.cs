﻿﻿namespace acmCrossroads.acmCrossroads
{
	using System;
	using PX.Data;
	
	[System.SerializableAttribute()]
	public class dvEsb : PX.Data.IBqlTable
	{
		#region EsbID
		public abstract class esbID : PX.Data.IBqlField
		{
		}
		protected int? _EsbID;
		[PXDBIdentity(IsKey = true)]
		[PXUIField(Enabled = false)]
		public virtual int? EsbID
		{
			get
			{
				return this._EsbID;
			}
			set
			{
				this._EsbID = value;
			}
		}
		#endregion
		#region EsbBaseUrl
		public abstract class esbBaseUrl : PX.Data.IBqlField
		{
		}
		protected string _EsbBaseUrl;
		[PXDBString(500)]
		[PXUIField(DisplayName = "EsbBaseUrl")]
		public virtual string EsbBaseUrl
		{
			get
			{
				return this._EsbBaseUrl;
			}
			set
			{
				this._EsbBaseUrl = value;
			}
		}
		#endregion
		#region EsbUsername
		public abstract class esbUsername : PX.Data.IBqlField
		{
		}
		protected string _EsbUsername;
		[PXDBString(500)]
		[PXUIField(DisplayName = "EsbUsername")]
		public virtual string EsbUsername
		{
			get
			{
				return this._EsbUsername;
			}
			set
			{
				this._EsbUsername = value;
			}
		}
		#endregion
		#region EsbPassword
		public abstract class esbPassword : PX.Data.IBqlField
		{
		}
		protected string _EsbPassword;
		[PXDBString(500)]
		[PXUIField(DisplayName = "EsbPassword")]
		public virtual string EsbPassword
		{
			get
			{
				return this._EsbPassword;
			}
			set
			{
				this._EsbPassword = value;
			}
		}
		#endregion
	}
}

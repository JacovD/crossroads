using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.AR;
using PX.Objects.CM;
using PX.Objects.Common.Discount.Attributes;
using PX.Objects.Common.Discount;
using PX.Objects.Common;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.DR;
using PX.Objects.GL.DAC.Abstract;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects.PM;
using PX.Objects.TX;
using PX.Objects;
using System.Collections.Generic;
using System.Diagnostics;
using System;

namespace PX.Objects.AR
{
    public class ARTranExt : PXCacheExtension<PX.Objects.AR.ARTran>
    {
        #region UsrFleetNumber
        [PXDBString(255)]
        [PXUIField(DisplayName = "Fleet Number")]

        public virtual string UsrFleetNumber { get; set; }
        public abstract class usrFleetNumber : IBqlField { }
        #endregion

        #region UsrInvoiceDescription 
        [PXDBString(255)]
        [PXUIField(DisplayName = "Invoice Description ")]

        public virtual string UsrInvoiceDescription { get; set; }
        public abstract class usrInvoiceDescription : IBqlField { }
        #endregion

        #region UsrConsignor
        [PXDBString(255)]
        [PXUIField(DisplayName = "Consignor")]

        public virtual string UsrConsignor { get; set; }
        public abstract class usrConsignor : IBqlField { }
        #endregion

        #region UsrConsignee
        [PXDBString(255)]
        [PXUIField(DisplayName = "Consignee")]

        public virtual string UsrConsignee { get; set; }
        public abstract class usrConsignee : IBqlField { }
        #endregion
    }

    [PXNonInstantiatedExtension]
    public class AR_ARTran_ExistingColumn : PXCacheExtension<PX.Objects.AR.ARTran>
    {
        #region SubID  
        [SubAccount(typeof(ARTran.accountID), typeof(ARTran.branchID), true, DisplayName = "Subaccount", Visibility = PXUIVisibility.Visible)]
        [PXDefault()]
        public int? SubID { get; set; }
        #endregion
    }
}
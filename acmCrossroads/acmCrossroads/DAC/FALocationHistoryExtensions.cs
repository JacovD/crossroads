using PX.Data;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.FA;
using PX.Objects.GL;
using PX.Objects;
using System.Collections.Generic;
using System;
using acmCrossroads.acmCrossroads;
using PX.Objects.AR;

namespace PX.Objects.FA
{
    public class FALocationHistoryExt : PXCacheExtension<PX.Objects.FA.FALocationHistory>
    {
        #region UsrReasonForPurchase
        [PXDBString(255)]
        [PXUIField(DisplayName = "Reason")]
        [PXSelector(typeof(Search<dvReasonForPurchase.reasonForPurchase, Where<dvReasonForPurchase.active, Equal<boolTrue>>>))]
        public virtual string UsrReasonForPurchase { get; set; }
        public abstract class usrReasonForPurchase : IBqlField { }
        #endregion

        #region UsrCreditTo
        [PXDBString(255)]
        [PXUIField(DisplayName = "Credit To")]
        [PXSelector(typeof(Search<Customer.acctCD>)
            , typeof(Customer.acctCD)
            , typeof(Customer.acctName)
            , DescriptionField = typeof(Customer.acctName))]

        public virtual string UsrCreditTo { get; set; }
        public abstract class usrCreditTo : IBqlField { }
        #endregion
    }
}
using CRLocation = PX.Objects.CR.Standalone.Location;
using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects.PM;
using PX.Objects.PO;
using PX.Objects;
using System.Collections.Generic;
using System;

namespace PX.Objects.PO
{
    public class POReceiptExt : PXCacheExtension<PX.Objects.PO.POReceipt>
    {

        #region UsrVendorGRN
        [PXDBString(255)]
        [PXUIField(DisplayName = "Vendor GRN")]

        public virtual string UsrVendorGRN { get; set; }
        public abstract class usrVendorGRN : IBqlField { }
        #endregion

        #region UsrGRNUsername

        [PXDBString(255)]
        [PXUIField(DisplayName = "Done By")]

        public virtual string UsrGRNUsername
        {
            get;
            set;
        }
        public abstract class usrGRNUsername : IBqlField { }
        #endregion

    }
}
using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.CM;
using PX.Objects.Common;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects.PM;
using PX.Objects.PO;
using PX.Objects.TX;
using PX.Objects;
using System.Collections.Generic;
using System;

namespace PX.Objects.PO
{
    public class POReceiptLineExt : PXCacheExtension<PX.Objects.PO.POReceiptLine>
    {
        #region UsrProjectVehicle
        [PXDBString(255)]
        [PXDefault("")]
        [PXUIField(DisplayName = "Fleet Number")]
        public virtual string UsrProjectVehicle { get; set; }
        public abstract class usrProjectVehicle : IBqlField { }
        #endregion
    }
}
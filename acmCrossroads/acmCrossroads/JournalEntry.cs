using PX.Data;
using PX.Objects.AP;
using PX.Objects.AR;
using PX.Objects.PO;
using PX.Objects.IN;
using System.Collections;

namespace PX.Objects.GL
{
    public class JournalEntry_Extension : PXGraphExtension<JournalEntry>
    { 
        #region Event Handlers
        protected void Batch_UsrUniqueExtReference_FieldVerifying(PXCache cache, PXFieldVerifyingEventArgs e)
        {
            Batch row = (Batch)e.Row;
            if (row != null)
            {
                Batch batch = PXSelect<Batch, Where<BatchExt.usrUniqueExtReference, Equal<Required<BatchExt.usrUniqueExtReference>>>>.SelectSingleBound(Base, null, e.NewValue.ToString());
                if (batch != null)
                {
                    if (!string.IsNullOrEmpty(batch.GetExtension<BatchExt>().UsrUniqueExtReference))
                    {
                        if (batch.GetExtension<BatchExt>().UsrUniqueExtReference.ToUpper() == e.NewValue.ToString().ToUpper())
                        {
                            e.Cancel = true;
                            string a = "Duplicate Unique Ext Reference(" + e.NewValue.ToString() + ").";
                            throw new PXException(a.ToString());
                        }
                    }
                }
            }
        }


        protected void GLTran_UsrFleetNumber_FieldDefaulting(PXCache cache, PXFieldDefaultingEventArgs e)
        {
            GLTran row = (GLTran)e.Row;
            if (row == null) return;
            GLTranExt itemExt = PXCache<GLTran>.GetExtension<GLTranExt>(row);
            if (itemExt == null) return;
            PXResultset<ARTran> aRTrans = PXSelect<ARTran,
                    Where<ARTran.tranType, Equal<Required<ARTran.tranType>>,
                        And<ARTran.refNbr, Equal<Required<ARTran.refNbr>>>>>
                //And<ARTran.lineNbr, Equal<Required<ARTran.lineNbr>
                .Select(Base, row.TranType, row.RefNbr);
            foreach (ARTran aRTran in aRTrans)
            {
                if (aRTran != null)
                {
                    ARTranExt aRTranExt = PXCache<ARTran>.GetExtension<ARTranExt>(aRTran);
                    if (aRTranExt != null)
                    {
                        itemExt.UsrFleetNumber = aRTranExt.UsrFleetNumber;
                        
                    }
                }
            }

            APTran aPTran = PXSelect<APTran,
                    Where<APTran.tranType, Equal<Required<APTran.tranType>>,
                        And<APTran.refNbr, Equal<Required<APTran.refNbr>>,
                            And<APTran.lineNbr, Equal<Required<APTran.lineNbr>>>>>>
                .Select(Base, row.TranType, row.RefNbr, row.TranLineNbr);
            if (aPTran != null)
            {
                APTranExt aPTranExt = PXCache<APTran>.GetExtension<APTranExt>(aPTran);
                if (aPTranExt != null)
                {
                    itemExt.UsrFleetNumber = aPTranExt.UsrFleetNumber;
                }
            }
            POReceipt PoRept = PXSelect<POReceipt,
                                          Where<POReceipt.invtRefNbr, Equal<Required<POReceipt.invtRefNbr>>>>
                                               .Select(Base, row.RefNbr);

            if (PoRept == null) { return; }

            POReceiptLine Rec = PXSelect<POReceiptLine,
                                   Where<POReceiptLine.receiptNbr, Equal<Required<POReceiptLine.receiptNbr>>>>
                                         .Select(Base, PoRept.ReceiptNbr);

            if (Rec != null)
            {
                POReceiptLineExt POReceiptLineExt = PXCache<POReceiptLine>.GetExtension<POReceiptLineExt>(Rec);

                if (POReceiptLineExt != null)
                {
                    itemExt.UsrFleetNumber = POReceiptLineExt.UsrProjectVehicle;
                }
            }

        }
        //public virtual void SaveBatchForDocument(JournalEntry je, ARRegister doc)
                          
        #endregion
    }
}
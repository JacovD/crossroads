using System;
using PX.Data;
using System.Collections;
using System.Collections.Generic;
using PX.Objects;
using PX.Objects.CM;
using acmCrossroads.Lib;
using acmCrossroads;
using RestSharp;

namespace PX.Objects.CM
{
    public class CuryRateMaint_Extension : PXGraphExtension<CuryRateMaint>
    {
        #region Event Handlers

        protected void CuryRateFilter_RowPersisting(PXCache cache, PXRowPersistingEventArgs e)
        {
            CuryRateFilter row = (CuryRateFilter)e.Row;
            string token = apiAuthentication.getToken(Base);
            if (!string.IsNullOrEmpty(token))
            {
                int companyID = PX.Data.Update.PXInstanceHelper.CurrentCompany;
                IRestResponse response = apiCuryRate.postCuryRate(token, Base, row, companyID);
                if (!RestSharpExtensionMethods.IsSuccessful(response))
                {
                    throw new PXException("Currency Rate Push Failed.");
                }
            }
            else
            {
                throw new PXException("Token Issue Failed.");
            }
        }
        #endregion
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using PX.Common;
using PX.Data;
using PX.SM;
using PX.Objects.AR.CCPaymentProcessing;
using PX.Objects.AR.Repositories;
using PX.Objects.Common;
using PX.Objects.Common.Discount;
using PX.Objects.CA;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.SO;
using PX.Objects.AR.CCPaymentProcessing.Helpers;
using CashAccountAttribute = PX.Objects.GL.CashAccountAttribute;
using PX.Objects.GL.Helpers;
using PX.Objects;
using PX.Objects.AR;
using acmCrossroads.Lib;
using RestSharp;
using acmCrossroads.acmCrossroads;

namespace PX.Objects.AR
{
    public class CustomerMaint_Extension : PXGraphExtension<CustomerMaint>
    {
        #region Event Handlers
        protected void Customer_RowPersisted(PXCache cache, PXRowPersistedEventArgs e)
        {
            Customer row = (Customer)e.Row;


            if (Base.BAccount.Cache.GetStatus(row) == PXEntryStatus.Deleted) return;
            dvEsb dv = abitGenix.GetESBLogin(Base);
            if (dv != null)
            {
                if (!string.IsNullOrEmpty(dv.EsbBaseUrl))
                {
                    if (row.BAccountID <= 0) return;
                    if (row.DefAddressID <= 0) return;
                    if (row.DefBillAddressID <= 0) return;
                    if (row.DefContactID <= 0) return;
                    if (row.DefLocationID <= 0) return;
                    string token = apiAuthentication.getToken(Base);
                    if (!string.IsNullOrEmpty(token))
                    {
                        int companyID = PX.Data.Update.PXInstanceHelper.CurrentCompany;
                        IRestResponse response = apiCustomer.postCustomer(token, Base, row, companyID);
                        if (!RestSharpExtensionMethods.IsSuccessful(response))
                        {
                            throw new PXException("Customer Push Failed.");
                        }
                        else
                        {
                            abitGenix.setLastSyncDate(Base, "Customer", row.BAccountID.ToString());
                            Base.BAccount.Cache.RaiseFieldUpdated<BAccountExt.usrCustomerLastSyncDate>(row, row.GetExtension<BAccountExt>().UsrCustomerLastSyncDate);
                        }
                    }
                }
            }
        }

        protected void Customer_UsrCustomerLastSyncDate_FieldUpdating(PXCache cache, PXFieldUpdatingEventArgs e)
        {
            Customer row = (Customer)e.Row;
            if (row != null)
            {
                if (row.BAccountID.HasValue)
                {
                    string d1 = row.GetExtension<BAccountExt>().UsrCustomerLastSyncDate ?? "";
                    string d2 = abitGenix.getLastSyncDate(Base, "Customer", row.BAccountID.ToString()) ?? "";
                    if (d1 != d2)
                    {
                        Base.BAccount.Cache.SetValue<BAccountExt.usrCustomerLastSyncDate>(row, abitGenix.getLastSyncDate(Base, "Customer", row.BAccountID.ToString()));
                    }
                }
            }
        }

        protected void Customer_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            Customer row = (Customer)e.Row;
            if (row != null)
            {
                string syncDate = abitGenix.getLastSyncDate(Base, "Customer", row.BAccountID.ToString());
                Base.BAccount.Cache.SetValueExt<BAccountExt.usrCustomerLastSyncDate>(row, syncDate);
            }
        }

        #endregion
    }

    public class syncTypeCustomer : Constant<string>
    {
        public syncTypeCustomer()
            : base("Customer")
        {
        }
    }
}
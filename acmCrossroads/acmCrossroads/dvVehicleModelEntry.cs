using System;
using System.Collections;
using System.Collections.Generic;
using PX.SM;
using PX.Data;
using acmCrossroads.Lib;
using RestSharp;


namespace acmCrossroads.acmCrossroads
{
    public class dvVehicleModelEntry : PXGraph<dvVehicleModelEntry>
    {
        public PXSelect<dvVehicleModel> VehicleModel;
        public PXSave<dvVehicleModel> Save;
        public PXCancel<dvVehicleModel> Cancel;

        #region Event Handlers

        //protected void VehicleModel_RowPersisting(PXCache cache, PXRowPersistingEventArgs e)
        //{
        //    dvVehicleModel row = (dvVehicleModel)e.Row;
        //    string token = apiAuthentication.getToken(this);
        //    if (!string.IsNullOrEmpty(token))
        //    {
        //        int companyID = PX.Data.Update.PXInstanceHelper.CurrentCompany;
        //        IRestResponse response = apiVehicleModel.postVehicleModel(token, this, row, companyID);
        //        if (!response.IsSuccessful)
        //        {
        //            throw new PXException("Vehicle Model Push Failed.");
        //        }
        //    }
        //}
        #endregion
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PX.Common;
using PX.Data;
using PX.Objects.CS;
using PX.Objects.CR;
using PX.Objects.CM;
using PX.Objects.GL;
using PX.Objects.GL.FinPeriods;
using PX.Objects.AP;
using PX.Objects.EP;
using PX.Objects.IN;
using PX.Objects.Common.Tools;
using PX.Objects.FA.Overrides.AssetProcess;
using PX.Objects.GL.FinPeriods.TableDefinition;
using PX.Objects;
using PX.Objects.FA;
using acmCrossroads.Lib;
using RestSharp;
using acmCrossroads.acmCrossroads;

namespace PX.Objects.FA
{
    public class AssetMaint_Extension : PXGraphExtension<AssetMaint>
    {
        #region Event Handlers
        static string rowname = null;
        static string rowStatus = null;
        static bool changed = false;
        protected void FixedAsset_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            FixedAsset row = (FixedAsset)e.Row;
            if (row == null)
            {
                return;
            }
            else
            {
                string syncDate = abitGenix.getLastSyncDate(Base, "FixedAsset", row.AssetID.ToString());
                Base.Asset.Cache.SetValueExt<FixedAssetExt.usrAssetProject>(row, row.AssetCD);
                Base.Asset.Cache.SetValueExt<FixedAssetExt.usrLastSyncDate>(row, syncDate);
            }
            if (row.AssetCD == null) { return; }

            if (Base.Accessinfo.ScreenID == "FA.50.60.00") return;
            if (Base.Accessinfo.ScreenID == "FA.30.30.PL") return;

            if (Base.Asset.Cache.GetStatus(row) == PXEntryStatus.Deleted) return;

            if (rowname != row.AssetCD.ToString() || rowname == null)
            {
                rowname = row.AssetCD.ToString();
                rowStatus = row.Status;
                changed = false;
            }
            else if (rowname == row.AssetCD.ToString())
            {
                if (rowStatus == row.Status)
                {
                    return;
                }
                else if (changed)
                {
                    return;
                }
                else
                {
                    changed = true;
                }
            }
            //TODO: removed comment 
            dvEsb dv = abitGenix.GetESBLogin(Base);
            //TODO: removed comment 
             if (dv != null)
            {   //TODO: removed comment 
                 if (!string.IsNullOrEmpty(dv.EsbBaseUrl))
                {
                    if (!row.ParentAssetID.HasValue && !row.SplittedFrom.HasValue)
                    {
                        bool isVehicle = false;
                        FADetails fADetails = PXSelect<FADetails, Where<FADetails.assetID, Equal<Required<FADetails.assetID>>>>.SelectSingleBound(Base, null, row.AssetID);
                        if (fADetails != null)
                        {
                            FADetailsExt detailsExt = fADetails.GetExtension<FADetailsExt>();


                            if (fADetails.PropertyType == "CC")
                            {
                                if (Base.LocationHistory != null)
                                {
                                    if (Base.LocationHistory.Current != null)
                                    {
                                        string customerToCredit = Base.LocationHistory.Current.GetExtension<FALocationHistoryExt>().UsrCreditTo ?? "";
                                        if (string.IsNullOrEmpty(customerToCredit))
                                        {
                                            throw new PXException("Credit To Required.");
                                        }
                                    }
                                }
                            }


                            if (detailsExt != null)
                            {
                                if (!string.IsNullOrEmpty(detailsExt.UsrAssetGroup))
                                {

                                    /* ---- Remove On Reqeust ---- */
                                    /*
                                        case "E": 
                                        {
                                            isVehicle = true;
                                            break;
                                        }
                                    */

                                    switch (detailsExt.UsrAssetGroup)
                                    {
                                        case "B":
                                            {
                                                isVehicle = true;
                                                break;
                                            }
                                        case "C":
                                            {
                                                isVehicle = true;
                                                break;
                                            }
                                        case "D":
                                            {
                                                isVehicle = true;
                                                break;
                                            }
                                        case "J":
                                            {
                                                isVehicle = true;
                                                break;
                                            }
                                        case "K":
                                            {
                                                isVehicle = true;
                                                break;
                                            }
                                        case "L":
                                            {
                                                isVehicle = true;
                                                break;
                                            }
                                        default:
                                            {
                                                isVehicle = false;
                                                break;
                                            }
                                    }

                                    if (isVehicle)
                                    {

                                        // Validation

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrAssetGroup))
                                        {
                                            throw new PXException("Vehicle Asset Group Required.");
                                        }

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrFleetNbr))
                                        {
                                            throw new PXException("Vehicle Fleet Number Required.");
                                        }

                                        if (!detailsExt.UsrFleetDate.HasValue)
                                        {
                                            throw new PXException("Vehicle Fleet Date Required.");
                                        }

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrYearModel))
                                        {
                                            throw new PXException("Vehicle Year Model Required.");
                                        }

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrEngineNbr))
                                        {
                                            throw new PXException("Vehicle Engine Nbr Required.");
                                        }

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrVehicleMake))
                                        {
                                            throw new PXException("Vehicle Make Required.");
                                        }

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrVehicleCategory))
                                        {
                                            throw new PXException("Vehicle Category Required.");
                                        }

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrVehicleModel))
                                        {
                                            throw new PXException("Vehicle Model Required.");
                                        }

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrVINNbr))
                                        {
                                            throw new PXException("Vehicle VIN Nbr Required.");
                                        }

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrRegistrationNbr))
                                        {
                                            throw new PXException("Vehicle Registration Nbr Required.");
                                        }

                                        //if (!(detailsExt.UsrIssueDate.HasValue))
                                        //{
                                        //    throw new PXException("Vehicle Issue Date Required.");
                                        //}

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrVehicleModel))
                                        {
                                            throw new PXException("Vehicle Model Required.");

                                        }
                                        else
                                        {
                                            dvVehicleModel vehicleModel = PXSelect<dvVehicleModel, Where<dvVehicleModel.vehicleModelID, Equal<Required<dvVehicleModel.vehicleModelID>>>>.SelectSingleBound(Base, null, detailsExt.UsrVehicleModel);
                                            if (vehicleModel == null)
                                            {
                                                throw new PXException("Vehicle model could not be found for the make and category.");
                                            }
                                        }
                                        //TODO: Remove below comment
                                           string token = apiAuthentication.getToken(Base);
                                        //TODO remove below 
                                        //string token = "fake";
                                        if (!string.IsNullOrEmpty(token))
                                        {
                                            int companyID = PX.Data.Update.PXInstanceHelper.CurrentCompany;

                                            IRestResponse response = apiVehicleFixedAsset.postVehicle(token, Base, row, companyID);

                                            //TODO: below comment needs to be removed and remove IF(1=2)
     
                                            if (response.StatusCode != System.Net.HttpStatusCode.OK)
                                            //if(1==2)
                                            {
                                                throw new PXException("Vehicle Push Failed.");
                                            }
                                            else
                                            {
                                                //added try catch becasue row select gives issues if you save. - jvd
                                                try
                                                {
                                                    abitGenix.setLastSyncDate(Base, "FixedAsset", row.AssetID.ToString());
                                                    Base.Asset.Cache.RaiseFieldUpdated<FixedAssetExt.usrLastSyncDate>(row, row.GetExtension<FixedAssetExt>().UsrLastSyncDate);
                                                }
                                                catch (Exception)
                                                {

                                                }
                                            }
                                        }
                                        else
                                        {
                                            throw new PXException("Token Issue Failed.");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void FixedAsset_RowPersisted(PXCache cache, PXRowPersistedEventArgs e)
        {

            if (Base.Accessinfo.ScreenID == "FA.50.60.00") return;
            FixedAsset row = (FixedAsset)e.Row;
            if (Base.Asset.Cache.GetStatus(row) == PXEntryStatus.Deleted) return;


            dvEsb dv = abitGenix.GetESBLogin(Base);
            // TODO: Die moet != wees - jvd
            //if (dv == null)
            if (dv != null)
            {
                // TODO: Die moet nie gecomment wees nie - jvd
                if (!string.IsNullOrEmpty(dv.EsbBaseUrl))
                {
                    if (!row.ParentAssetID.HasValue && !row.SplittedFrom.HasValue)
                    {
                        bool isVehicle = false;
                        FADetails fADetails = PXSelect<FADetails, Where<FADetails.assetID, Equal<Required<FADetails.assetID>>>>.SelectSingleBound(Base, null, row.AssetID);
                        if (fADetails != null)
                        {
                            FADetailsExt detailsExt = fADetails.GetExtension<FADetailsExt>();


                            if (fADetails.PropertyType == "CC")
                            {
                                if (Base.LocationHistory != null)
                                {
                                    if (Base.LocationHistory.Current != null)
                                    {
                                        string customerToCredit = Base.LocationHistory.Current.GetExtension<FALocationHistoryExt>().UsrCreditTo ?? "";
                                        if (string.IsNullOrEmpty(customerToCredit))
                                        {
                                            throw new PXException("Credit To Required.");
                                        }
                                    }
                                }
                            }


                            if (detailsExt != null)
                            {
                                if (!string.IsNullOrEmpty(detailsExt.UsrAssetGroup))
                                {

                                    /* ---- Remove On Reqeust ---- */
                                    /*
                                        case "E": 
                                        {
                                            isVehicle = true;
                                            break;
                                        }
                                    */

                                    switch (detailsExt.UsrAssetGroup)
                                    {
                                        case "B":
                                            {
                                                isVehicle = true;
                                                break;
                                            }
                                        case "C":
                                            {
                                                isVehicle = true;
                                                break;
                                            }
                                        case "D":
                                            {
                                                isVehicle = true;
                                                break;
                                            }
                                        case "J":
                                            {
                                                isVehicle = true;
                                                break;
                                            }
                                        case "K":
                                            {
                                                isVehicle = true;
                                                break;
                                            }
                                        case "L":
                                            {
                                                isVehicle = true;
                                                break;
                                            }
                                        default:
                                            {
                                                isVehicle = false;
                                                break;
                                            }
                                    }

                                    if (isVehicle)
                                    {

                                        // Validation

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrAssetGroup))
                                        {
                                            throw new PXException("Vehicle Asset Group Required.");
                                        }

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrFleetNbr))
                                        {
                                            throw new PXException("Vehicle Fleet Number Required.");
                                        }

                                        if (!detailsExt.UsrFleetDate.HasValue)
                                        {
                                            throw new PXException("Vehicle Fleet Date Required.");
                                        }

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrYearModel))
                                        {
                                            throw new PXException("Vehicle Year Model Required.");
                                        }

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrEngineNbr))
                                        {
                                            throw new PXException("Vehicle Engine Nbr Required.");
                                        }

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrVehicleMake))
                                        {
                                            throw new PXException("Vehicle Make Required.");
                                        }

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrVehicleCategory))
                                        {
                                            throw new PXException("Vehicle Category Required.");
                                        }

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrVehicleModel))
                                        {
                                            throw new PXException("Vehicle Model Required.");
                                        }

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrVINNbr))
                                        {
                                            throw new PXException("Vehicle VIN Nbr Required.");
                                        }

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrRegistrationNbr))
                                        {
                                            throw new PXException("Vehicle Registration Nbr Required.");
                                        }

                                        //if (!(detailsExt.UsrIssueDate.HasValue))
                                        //{
                                        //    throw new PXException("Vehicle Issue Date Required.");
                                        //}

                                        if (string.IsNullOrWhiteSpace(detailsExt.UsrVehicleModel))
                                        {
                                            throw new PXException("Vehicle Model Required.");

                                        }
                                        else
                                        {
                                            dvVehicleModel vehicleModel = PXSelect<dvVehicleModel, Where<dvVehicleModel.vehicleModelID, Equal<Required<dvVehicleModel.vehicleModelID>>>>.SelectSingleBound(Base, null, detailsExt.UsrVehicleModel);
                                            if (vehicleModel == null)
                                            {
                                                throw new PXException("Vehicle model could not be found for the make and category.");
                                            }
                                        }
                                        // TODO: code moet nie gecomment wees nie
                                        string token = apiAuthentication.getToken(Base);
                                        //string token = "TEST";
                                        if (!string.IsNullOrEmpty(token))
                                        {
                                            int companyID = PX.Data.Update.PXInstanceHelper.CurrentCompany;

                                            IRestResponse response = apiVehicleFixedAsset.postVehicle(token, Base, row, companyID);

                                            // TODO remove comment 
                                            if (response.StatusCode != System.Net.HttpStatusCode.OK)
                                            {
                                                throw new PXException("Vehicle Push Failed.");
                                            }
                                            else
                                            {
                                                abitGenix.setLastSyncDate(Base, "FixedAsset", row.AssetID.ToString());
                                                Base.Asset.Cache.RaiseFieldUpdated<FixedAssetExt.usrLastSyncDate>(row, row.GetExtension<FixedAssetExt>().UsrLastSyncDate);
                                            }
                                        }
                                        else
                                        {
                                            throw new PXException("Token Issue Failed.");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void FixedAsset_ClassID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            FixedAsset row = (FixedAsset)e.Row;
            if (row != null)
            {
                if (row.ClassID.HasValue)
                {
                    FixedAsset fAClass = PXSelect<FixedAsset, Where<FixedAsset.assetID, Equal<Required<FixedAsset.assetID>>, And<FixedAsset.recordType, Equal<FARecordType.classType>>>>.SelectSingleBound(Base, null, row.ClassID);
                    if (fAClass != null)
                    {
                        if (!string.IsNullOrEmpty(fAClass.AssetCD))
                        {
                            Base.AssetDetails.Cache.SetValueExt<FADetailsExt.usrAssetGroup>(Base.AssetDetails.Current, fAClass.AssetCD[0]);
                        }
                    }
                }
            }
        }

        protected void FixedAsset_UsrLastSyncDate_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            FixedAsset row = (FixedAsset)e.Row;
            if (row != null)
            {
                if (row.AssetID.HasValue)
                {
                    string d1 = row.GetExtension<FixedAssetExt>().UsrLastSyncDate ?? "";
                    string d2 = abitGenix.getLastSyncDate(Base, "FixedAsset", row.AssetID.ToString()) ?? "";
                    if (d1 != d2)
                    {
                        Base.Asset.Cache.SetValueExt<FixedAssetExt.usrLastSyncDate>(row, d2);
                    }
                }
            }

        }

        protected void FixedAsset_AssetCD_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            FixedAsset row = (FixedAsset)e.Row;
            if (row != null)
            {
                Base.Asset.Cache.SetValueExt<FixedAssetExt.usrAssetProject>(row, row.AssetCD);
            }
        }

        protected void FADetails_UsrVehicleModel_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e)
        {
            FADetails row = (FADetails)e.Row;
            FADetailsExt detailsExt = row.GetExtension<FADetailsExt>();
            if (!detailsExt.UsrTankCapacity.HasValue)
            {
                if (!string.IsNullOrEmpty(detailsExt.UsrVehicleModel))
                {
                    dvVehicleModel vehicleModel = PXSelect<dvVehicleModel, Where<dvVehicleModel.vehicleModelID, Equal<Required<dvVehicleModel.vehicleModelID>>>>.SelectSingleBound(Base, null, detailsExt.UsrVehicleModel);
                    if (vehicleModel != null)
                    {
                        if (vehicleModel.FuelTankCapacity.HasValue)
                        {
                            Base.AssetDetails.SetValueExt<FADetailsExt.usrTankCapacity>(row, (int)vehicleModel.FuelTankCapacity);
                        }
                    }
                }
            }
        }

        #endregion

        #region Acumatica Support[Case #091130]
        protected virtual void FADetails_DepreciateFromDate_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e, PXFieldUpdated del)
        {
            var asset = Base.Asset.Current;

            Base.Asset.Current = GetAsset((FADetails)e.Row);

            del(sender, e);

            Base.Asset.Current = asset;
        }

        protected virtual void FADetails_AcquisitionCost_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e, PXFieldUpdated del)
        {
            var asset = Base.Asset.Current;

            Base.Asset.Current = GetAsset((FADetails)e.Row);

            del(sender, e);

            Base.Asset.Current = asset;
        }

        protected virtual void FADetails_SalvageAmount_FieldUpdated(PXCache sender, PXFieldUpdatedEventArgs e, PXFieldUpdated del)
        {
            var asset = Base.Asset.Current;

            Base.Asset.Current = GetAsset((FADetails)e.Row);

            del(sender, e);

            Base.Asset.Current = asset;
        }

        protected FixedAsset GetAsset(FADetails details)
        {
            return PXSelect<FixedAsset, Where<FixedAsset.assetID, Equal<Required<FixedAsset.assetID>>>>.Select(Base,
                details.AssetID);
        }

        #endregion


    }
}
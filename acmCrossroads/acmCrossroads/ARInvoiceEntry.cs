using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using PX.Common;
using PX.Objects.Common;
using PX.Objects.Common.Discount;
using PX.Data;
using PX.Data.DependencyInjection;
using PX.Objects.GL;
using PX.Objects.CM;
using PX.Objects.CS;
using PX.Objects.CR;
using PX.Objects.TX;
using PX.Objects.IN;
using PX.Objects.BQLConstants;
using PX.Objects.EP;
using PX.Objects.SO;
using PX.Objects.DR;
using SOInvoiceEntry = PX.Objects.SO.SOInvoiceEntry;
using CRLocation = PX.Objects.CR.Standalone.Location;
using PX.Objects.GL.Reclassification.UI;
using PX.Objects.AR.BQL;
using PX.Objects.Common.Extensions;
using PX.Objects.PM;
using System.Text;
using PX.Objects.Common.Bql;
using PX.Objects.GL.FinPeriods;
using PX.Objects;
using PX.Objects.AR;

namespace PX.Objects.AR
{
    public class ARInvoiceEntry_Extension : PXGraphExtension<ARInvoiceEntry>
    {
        #region Event Handlers

        public delegate IEnumerable ReleaseDelegate(PXAdapter adapter);
        [PXOverride]
        public IEnumerable Release(PXAdapter adapter, ReleaseDelegate baseMethod)
        {
            baseMethod(adapter);
            return adapter.Get();
        }

        protected void ARTran_SubID_FieldDefaulting(PXCache cache, PXFieldDefaultingEventArgs e, PXFieldDefaulting InvokeBaseHandler)
        {

            ARTran row = (ARTran)e.Row;
            if (row == null) { return; }
            Base.Transactions.Cache.SetValueExt<ARTran.subID>(row, null);
            //if (row.SubID.HasValue)
            //{
            //    Base.Transactions.SetValueExt<ARTran.subID>(row, null);
            //}
            //if (InvokeBaseHandler != null)
            //    InvokeBaseHandler(cache, e);

        }
        protected void ARTran_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {
            var row = (ARTran)e.Row;
            PXUIFieldAttribute.SetEnabled<ARTran.tranDesc>(cache, null, true);

        }


        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using PX.Objects.GL;
using RestSharp;
using acmCrossroads.Lib;
using acmCrossroads.acmCrossroads;

namespace acmCrossroads.Lib
{
    public class apiSubAccount
    {
        public static IRestResponse postSubAccount(string authToken, SubAccountMaint Base, Sub SubAccount, int CompanyID)
        {
            dvEsb dv = abitGenix.GetESBLogin(Base);
            var client = new RestClient(dv.EsbBaseUrl + "CrdEsb/api/Acumatica/SubAccount");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Token", authToken);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(new
            {
                CountryID = abitGenix.getCountryID(Base, Base.Accessinfo.CompanyName, CompanyID),
                company = CompanyID,
                channelCode = SubAccount.SubCD,
                channelDescription = SubAccount.Description,
                isActive = SubAccount.Active
            });
            IRestResponse response = client.Execute(request);
            return response;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using acmCrossroads.acmCrossroads;
using RestSharp;
using PX.Data;

namespace acmCrossroads.Lib
{
    class apiVehicleCategory
    {
        /*
         "countryId": 0,
         "vehicleCategory": "string",
         "glMapping": "string",
         "cofRequired": true,
         "isActive": true
         */

        public static IRestResponse postVehicleCategory(string authToken, dvVehicleCategoryMaint Base, dvVehicleCategory row, int CompanyID)
        {
            dvEsb dv = abitGenix.GetESBLogin(Base);
            var client = new RestClient(dv.EsbBaseUrl + "CrdEsb/api/Acumatica/VehicleCategories");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Token", authToken);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(clsVehicleCategories(Base, row, CompanyID));
            IRestResponse response = client.Execute(request);
            return response;
        }

        private static List<clsVehicleCategory> clsVehicleCategories(dvVehicleCategoryMaint Base, dvVehicleCategory row, int CompanyID)
        {
            List<clsVehicleCategory> list = new List<clsVehicleCategory>();
            if (row != null)
            {
                clsVehicleCategory item = new clsVehicleCategory();
                item.CountryID = abitGenix.getCountryID(Base, Base.Accessinfo.CompanyName, CompanyID);
                item.vehicleCategory = row.VehicleCategory;
                item.glMapping = row.AssetGroup;
                item.cofRequired = row.VehicleCategoryCOF;
                item.isActive = row.Active ?? false;
                list.Add(item);
            }
            return list;
        }

    }

    public class clsVehicleCategory
    {
        public int? CountryID { get; set; }
        public string vehicleCategory { get; set; }
        public string glMapping { get; set; }
        public string cofRequired { get; set; }
        public bool isActive { get; set; }
    }
}

﻿using acmCrossroads.acmCrossroads;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.CA;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.TX;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace acmCrossroads.Lib
{
    class apiVendor
    {
        public static IRestResponse postVendor(string authToken, VendorMaint Base, Vendor row, int CompanyID)
        {
            dvEsb dv = abitGenix.GetESBLogin(Base);
            var client = new RestClient(dv.EsbBaseUrl + "CrdEsb/api/Acumatica/Supplier");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Token", authToken);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(getPostBody(Base, row, CompanyID));
            IRestResponse response = client.Execute(request);
            return response;
        }

        public static List<clsVendor> getPostBody(VendorMaint Base, Vendor row, int CompanyID)
        {

            List<clsVendor> list = new List<clsVendor>();

            if (row != null)
            {
                Address phyAddress = PXSelect<Address, Where<Address.addressID, Equal<Required<Address.addressID>>>>.SelectSingleBound(Base, null, row.DefAddressID);
                Location locationDetail = PXSelect<Location, Where<Location.locationID, Equal<Required<Location.locationID>>>>.SelectSingleBound(Base, null, row.DefLocationID);

                Address postAddress = PXSelect<Address, Where<Address.addressID, Equal<Required<Address.addressID>>>>.SelectSingleBound(Base, null, locationDetail.VRemitAddressID);
                if (postAddress == null && phyAddress != null)
                {
                    postAddress = phyAddress;
                }
                Contact contactDetail = PXSelect<Contact, Where<Contact.contactID, Equal<Required<Contact.contactID>>>>.SelectSingleBound(Base, null, row.DefContactID);
                string sCompanyRegistrationNumber = "";
                PaymentMethodDetail paymentMethodDetail = PXSelect<PaymentMethodDetail, Where<PaymentMethodDetail.paymentMethodID, Equal<Required<PaymentMethodDetail.paymentMethodID>>, And<PaymentMethodDetail.descr, Equal<Required<PaymentMethodDetail.descr>>>>>.SelectSingleBound(Base, null, locationDetail.PaymentMethodID, "Registration Number");
                VendorPaymentMethodDetail vendorPaymentMethodDetail = new VendorPaymentMethodDetail();
                if (paymentMethodDetail != null)

                {
                    vendorPaymentMethodDetail = PXSelect<VendorPaymentMethodDetail,
                                                Where<VendorPaymentMethodDetail.bAccountID, Equal<Required<VendorPaymentMethodDetail.locationID>>,
                                                And<VendorPaymentMethodDetail.detailID, Equal<Required<VendorPaymentMethodDetail.detailID>
                                                >>>>.SelectSingleBound(Base, null, row.DefLocationID, paymentMethodDetail.DetailID);
                    if (vendorPaymentMethodDetail != null)
                    {
                        sCompanyRegistrationNumber = vendorPaymentMethodDetail.DetailValue;
                    }
                }
                string sTax = "";
                TaxZone taxZone = PXSelect<TaxZone, Where<TaxZone.taxZoneID, Equal<Required<TaxZone.taxZoneID>>>>.SelectSingleBound(Base, null, locationDetail.VTaxZoneID);
                if (taxZone != null)
                {
                    if (!string.IsNullOrEmpty(taxZone.DfltTaxCategoryID))
                    {
                        sTax = taxZone.DfltTaxCategoryID;
                    }
                }

                /*Supplier Group*/
                string sSupplierGroup = "";
                CSAnswers answers = PXSelect<CSAnswers, Where<CSAnswers.refNoteID, Equal<Required<CSAnswers.refNoteID>>, And<CSAnswers.attributeID, Equal<Required<CSAnswers.attributeID>>>>>.SelectSingleBound(Base, null, row.NoteID, "SUPPLGROUP");
                CSAttributeDetail attributeDetail = new CSAttributeDetail();
                if (answers != null)
                {
                    attributeDetail = PXSelect<CSAttributeDetail, Where<CSAttributeDetail.valueID, Equal<Required<CSAttributeDetail.valueID>>, And<CSAttributeDetail.attributeID, Equal<Required<CSAttributeDetail.attributeID>>>>>.SelectSingleBound(Base, null, answers.Value, "SUPPLGROUP");
                    if (attributeDetail != null)
                    {
                        sSupplierGroup = attributeDetail.Description ?? "";
                    }
                }

                /*Subbie*/
                bool result = false;
                string sSubbie = "False";
                CSAnswers answersSubbie = PXSelect<CSAnswers, Where<CSAnswers.refNoteID, Equal<Required<CSAnswers.refNoteID>>,
                                            And<CSAnswers.attributeID, Equal<Required<CSAnswers.attributeID>>>>>.SelectSingleBound(Base, null, row.NoteID, "SUBBIE");
                CSAttributeDetail attributeDetailSubbie = new CSAttributeDetail();
                if (answersSubbie != null)
                {
                    sSubbie = answersSubbie.Value;
                    if (answersSubbie.Value == "1")
                    {
                        result = true;
                    }
                    /*attributeDetailSubbie = PXSelect<CSAttributeDetail, Where<CSAttributeDetail.valueID, Equal<Required<CSAttributeDetail.valueID>>, And<CSAttributeDetail.attributeID, Equal<Required<CSAttributeDetail.attributeID>>>>>.SelectSingleBound(Base, null, answers.Value, "SUBBIE");
                    if (attributeDetailSubbie != null)
                    {
                        sSubbie = attributeDetailSubbie.ValueID ?? "0";
                    }*/
                }
                bool isActive = false;
                if (row.Status == "A")
                {
                    isActive = true;
                }


                clsVendor item = new clsVendor();
                item.CountryID = abitGenix.getCountryID(Base, Base.Accessinfo.CompanyName, CompanyID);
                item.SupplierCode = row.AcctCD.ToString().Trim();
                item.SupplierName = row.AcctName.Trim();
                item.PhysicalAddressLine1 = phyAddress.AddressLine1 ?? "";
                item.PhysicalAddressLine2 = phyAddress.AddressLine2 ?? "";
                item.PhysicalAddressLine3 = phyAddress.AddressLine3 ?? "";
                item.PhysicalAddressLine4 = phyAddress.City ?? "";
                item.PhysicalAddressAreaCode = phyAddress.PostalCode;
                item.PostalAddressLine1 = postAddress.AddressLine1 ?? "";
                item.PostalAddressLine2 = postAddress.AddressLine2 ?? "";
                item.PostalAddressLine3 = postAddress.AddressLine3 ?? "";
                item.PostalAddressLine4 = postAddress.City ?? phyAddress.City ?? "";
                item.PostalCode = postAddress.PostalCode ?? "";
                item.RegionStateProvinceCode = phyAddress.State ?? "";
                item.CompanyRegistrationNumber = sCompanyRegistrationNumber ?? "";
                item.CompanyVatRegistrationNumber = locationDetail.TaxRegistrationID ?? "";
                item.CompanyTelephoneNumber = contactDetail.Phone1 ?? "";
                item.CompanyFaxNumber = contactDetail.Fax ?? "";
                item.CompanyGLCode = abitGenix.GetAccountCD(Base, locationDetail.VAPAccountID) ?? "";
                item.SupplierGroup = sSupplierGroup ?? "";
                item.VatCode = locationDetail.VTaxZoneID ?? "";
                item.CompanyDefaultCurrency = row.CuryID ?? "";
                item.CompanyDefaultTaxCode = sTax;
                //bool.TryParse(sSubbie, out result);
                item.IsSubContractor = result;
                item.IsActive = isActive;
                list.Add(item);
            }

            return list;
        }

    }

    public class clsVendor
    {
        public int? CountryID { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string PhysicalAddressLine1 { get; set; }
        public string PhysicalAddressLine2 { get; set; }
        public string PhysicalAddressLine3 { get; set; }
        public string PhysicalAddressLine4 { get; set; }
        public string PhysicalAddressAreaCode { get; set; }
        public string PostalAddressLine1 { get; set; }
        public string PostalAddressLine2 { get; set; }
        public string PostalAddressLine3 { get; set; }
        public string PostalAddressLine4 { get; set; }
        public string PostalCode { get; set; }
        public string RegionStateProvinceCode { get; set; }
        public string CompanyRegistrationNumber { get; set; }
        public string CompanyVatRegistrationNumber { get; set; }
        public string CompanyTelephoneNumber { get; set; }
        public string CompanyFaxNumber { get; set; }
        public string CompanyCreditAvailable { get; set; }
        public string CompanyCreditLimit { get; set; }
        public string CompanyGLCode { get; set; }
        public string SupplierGroup { get; set; }
        public string VatCode { get; set; }
        public string CompanyDefaultTaxCode { get; set; }
        public string CompanyDefaultCurrency { get; set; }
        public bool IsSubContractor { get; set; }
        public bool IsActive { get; set; }
    }
}

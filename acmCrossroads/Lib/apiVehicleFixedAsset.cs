﻿using PX.Objects.FA;
using System.Net;
using System.Net.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using PX.Data;
using PX.Data.Update;
using acmCrossroads.Lib;
using acmCrossroads.acmCrossroads;

namespace acmCrossroads.Lib
{
    public class apiVehicleFixedAsset
    {
        public static IRestResponse postVehicle(string authToken, AssetMaint Base, FixedAsset row, int CompanyID)
        {

            try
            {
                // TODO: below should not there
                 var a = getPostBody(Base, row, CompanyID);
                //
                dvEsb dv = abitGenix.GetESBLogin(Base);
                var client = new RestClient(dv.EsbBaseUrl + "CrdEsb/api/Acumatica/FixedAsset");
                var request = new RestRequest(Method.POST);

                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Token", authToken);
                request.RequestFormat = DataFormat.Json;
                request.AddBody(getPostBody(Base, row, CompanyID));
                IRestResponse response = client.Execute(request);
                return response;
            }
            catch (Exception ex)
            {
                //TODO below should not be commented
                //throw ex;
                //TODO below code line should not be there
                return null;
                //
            }
        }


        private static List<clsVehicleFixedAsset> getPostBody(PXGraph Base, FixedAsset row, int CompanyID)
        {
            List<clsVehicleFixedAsset> list = new List<clsVehicleFixedAsset>();
            if (row != null)
            {
                FADetails fADetails = PXSelect<FADetails, Where<FADetails.assetID, Equal<Required<FADetails.assetID>>>>.SelectSingleBound(Base, null, row.AssetID);
                FADetailsExt detailsExt = fADetails.GetExtension<FADetailsExt>();
                FALocationHistory fALocationHistory = PXSelect<FALocationHistory, Where<FALocationHistory.assetID, Equal<Required<FALocationHistory.assetID>>>, OrderBy<Desc<FALocationHistory.lastModifiedDateTime>>>.SelectSingleBound(Base, null, row.AssetID);
                FALocationHistoryExt locationHistoryExt = fALocationHistory.GetExtension<FALocationHistoryExt>();
                dvVehicleModel vehicleModel = PXSelect<dvVehicleModel, Where<dvVehicleModel.vehicleModelID, Equal<Required<dvVehicleModel.vehicleModelID>>>>.SelectSingleBound(Base, null, detailsExt.UsrVehicleModel);
                if (vehicleModel == null)
                {
                    throw new PXException("Vehicle model could not be found.");
                }
                dvVehicleMake vehicleMake = PXSelect<dvVehicleMake, Where<dvVehicleMake.vehicleMakeCode, Equal<Required<dvVehicleMake.vehicleMakeCode>>>>.SelectSingleBound(Base, null, vehicleModel.VehicleMake);
                if (vehicleMake == null)
                {
                    throw new PXException("Vehicle Make could not be found.");
                }

                bool isActive = false;
                // if (row.Status == FixedAssetStatus.Active || row.Status == FixedAssetStatus.FullyDepreciated || row.Status == FixedAssetStatus.Reversed)
                if (row.Status == FixedAssetStatus.Active || row.Status == FixedAssetStatus.FullyDepreciated || row.Status == FixedAssetStatus.Hold)
                {
                    isActive = true;
                }

                clsVehicleFixedAsset item = new clsVehicleFixedAsset();
                item.countryId = abitGenix.getCountryID(Base, Base.Accessinfo.CompanyName, CompanyID);
                item.accumulatedDepreciationAccount = abitGenix.GetAccountCD(Base, row.AccumulatedDepreciationAccountID);
                item.accumulatedDepreciationSub = abitGenix.GetSubAccountCD(Base, row.AccumulatedDepreciationSubID);
                item.assetGroup = abitGenix.GetFixedAssetClassCD(Base, row.ClassID);
                item.assetID = row.AssetCD;
                item.auxiliaryEquipment = detailsExt.UsrAuxiliaryEquipment;
                item.auxiliaryEquipmentValue = detailsExt.UsrAuxiliaryEquipmentValue;
                item.billNumber = fADetails.BillNumber;
                item.branch = row.BranchID.ToString();
                item.building = fALocationHistory.BuildingID.ToString();
                item.condition = fADetails.Condition;
                item.configuration = detailsExt.UsrConfiguration;
                item.constructionAccount = abitGenix.GetAccountCD(Base, row.ConstructionAccountID);
                item.constructionSub = abitGenix.GetSubAccountCD(Base, row.ConstructionSubID);
                item.custodian = fALocationHistory.Custodian.ToString();
                item.department = fALocationHistory.Department;
                item.depreciationExpenseAccount = abitGenix.GetAccountCD(Base, row.DepreciatedExpenseAccountID);
                item.depreciationExpenseSub = abitGenix.GetSubAccountCD(Base, row.DepreciatedExpenseSubID);
                item.description = row.Description.Trim();
                item.disposalAmount = fADetails.SaleAmount;
                item.disposalDate = fADetails.DisposalDate;
                item.disposalMethod = fADetails.DisposalPeriodID;
                item.engineNumber = detailsExt.UsrEngineNbr;
                item.fAAccrualAccount = abitGenix.GetAccountCD(Base, row.FAAccrualAcctID);
                item.fAAccrualSub = abitGenix.GetSubAccountCD(Base, row.FAAccrualSubID);
                item.fairMarketValue = fADetails.FairMarketValue;
                item.fixedAssetsAccount = abitGenix.GetAccountCD(Base, fALocationHistory.FAAccountID);
                item.fixedAssetsSub = abitGenix.GetSubAccountCD(Base, fALocationHistory.FASubID);
                item.fleetDate = detailsExt.UsrFleetDate;
                item.fleetNumber = detailsExt.UsrFleetNbr;
                item.floor = fALocationHistory.Floor;
                item.gainAccount = abitGenix.GetAccountCD(Base, fALocationHistory.GainAcctID);
                item.gainSub = abitGenix.GetSubAccountCD(Base, fALocationHistory.GainSubID);
                item.insurance = detailsExt.UsrInsurance;
                item.issueDate = detailsExt.UsrIssueDate;
                item.iTNumber = detailsExt.UsrITNbr;
                item.leaseAccount = abitGenix.GetAccountCD(Base, row.LeaseAccountID);
                item.leaseNumber = fADetails.LeaseNumber;
                item.leaseRentTermmonths = fADetails.LeaseRentTerm;
                item.leaseSub = abitGenix.GetSubAccountCD(Base, row.LeaseSubID);
                item.lessor = abitGenix.GetFixedAssetLessor(Base, fADetails.LessorID);
                item.licenceNumberasperRC1 = detailsExt.UsrAssetNbr2;
                item.lossAccount = abitGenix.GetAccountCD(Base, row.LossAcctID);
                item.lossSub = abitGenix.GetSubAccountCD(Base, row.LossSubID);
                item.manufacturer = fADetails.Manufacturer;
                item.manufacturingYear = fADetails.ManufacturingYear;
                item.marketValue = detailsExt.UsrMarketValue;
                item.model = vehicleModel.ModelCode;
                item.origAcquisitionCost = 0;
                item.parentAsset = "";
                item.personalPropertyType = "";
                item.placedinServiceDate = fADetails.DepreciateFromDate;
                item.pONumber = fADetails.PONumber;
                item.pQMarketValue = detailsExt.UsrPQMarketValue;
                item.proceedsAccount = abitGenix.GetAccountCD(Base, row.DisposalAccountID);
                item.proceedsSub = abitGenix.GetSubAccountCD(Base, row.DisposalSubID);
                item.reason = locationHistoryExt.UsrReasonForPurchase;
                item.receiptDate = fADetails.ReceiptDate;
                item.registrationNumber = detailsExt.UsrRegistrationNbr;
                item.rentAccount = abitGenix.GetAccountCD(Base, row.RentAccountID);
                item.rentAmount = fADetails.RentAmount;
                item.rentSub = abitGenix.GetSubAccountCD(Base, row.RentSubID);
                item.replacementCost = fADetails.ReplacementCost;
                item.residualValue = detailsExt.UsrResidualValue;
                item.retailCost = fADetails.RetailCost;
                item.room = fALocationHistory.Room;
                item.salvageAmount = fADetails.SalvageAmount;
                item.serialNumber = fADetails.SerialNumber;
                item.vehicleCategory = detailsExt.UsrVehicleCategory;
                item.vehicleMake = detailsExt.UsrVehicleMake;
                item.modelVariant = vehicleModel.ModelVariant;
                item.modelCode = vehicleModel.ModelCode;
                item.vINNumber = detailsExt.UsrVINNbr;
                item.warrantyExpiresOn = fADetails.WarrantyExpirationDate;
                item.yearModel = detailsExt.UsrYearModel;
                item.status = isActive;
                item.tankCapacity = detailsExt.UsrTankCapacity;
                item.customerCode = locationHistoryExt.UsrCreditTo;
                item.vehicleMakeDescription = vehicleMake.VehicleMakeDescription;
                item.modelDescription = vehicleModel.ModelDescription;
                list.Add(item);

            }
            return list;
        }
    }

    public class clsVehicleFixedAsset
    {
        public int countryId { get; set; }
        public string accumulatedDepreciationAccount { get; set; }
        public string accumulatedDepreciationSub { get; set; }
        public string assetGroup { get; set; }
        public string assetID { get; set; }
        public string auxiliaryEquipment { get; set; }
        public decimal? auxiliaryEquipmentValue { get; set; }
        public string billNumber { get; set; }
        public string branch { get; set; }
        public DateTime? buildDate { get; set; }
        public string building { get; set; }
        public string condition { get; set; }
        public string configuration { get; set; }
        public string constructionAccount { get; set; }
        public string constructionSub { get; set; }
        public string custodian { get; set; }
        public string department { get; set; }
        public string depreciationExpenseAccount { get; set; }
        public string depreciationExpenseSub { get; set; }
        public string description { get; set; }
        public decimal? disposalAmount { get; set; }
        public DateTime? disposalDate { get; set; }
        public string disposalMethod { get; set; }
        public string engineNumber { get; set; }
        public string fAAccrualAccount { get; set; }
        public string fAAccrualSub { get; set; }
        public decimal? fairMarketValue { get; set; }
        public string fixedAssetsAccount { get; set; }
        public string fixedAssetsSub { get; set; }
        public DateTime? fleetDate { get; set; }
        public string fleetNumber { get; set; }
        public string floor { get; set; }
        public string gainAccount { get; set; }
        public string gainSub { get; set; }
        public string insurance { get; set; }
        public DateTime? issueDate { get; set; }
        public string iTNumber { get; set; }
        public string leaseAccount { get; set; }
        public string leaseNumber { get; set; }
        public int? leaseRentTermmonths { get; set; }
        public string leaseSub { get; set; }
        public string lessor { get; set; }
        public string licenceNumberasperRC1 { get; set; }
        public string lossAccount { get; set; }
        public string lossSub { get; set; }
        public string manufacturer { get; set; }
        public string manufacturingYear { get; set; }
        public decimal? marketValue { get; set; }
        public string model { get; set; }
        public decimal? origAcquisitionCost { get; set; }
        public string parentAsset { get; set; }
        public string personalPropertyType { get; set; }
        public DateTime? placedinServiceDate { get; set; }
        public string pONumber { get; set; }
        public decimal? pQMarketValue { get; set; }
        public string proceedsAccount { get; set; }
        public string proceedsSub { get; set; }
        public string reason { get; set; }
        public DateTime? receiptDate { get; set; }
        public string registrationNumber { get; set; }
        public string rentAccount { get; set; }
        public decimal? rentAmount { get; set; }
        public string rentSub { get; set; }
        public decimal? replacementCost { get; set; }
        public decimal? residualValue { get; set; }
        public decimal? retailCost { get; set; }
        public string room { get; set; }
        public decimal? salvageAmount { get; set; }
        public string serialNumber { get; set; }
        public string vehicleCategory { get; set; }
        public string vehicleMake { get; set; }
        public string modelVariant { get; set; }
        public string modelCode { get; set; }
        public string vINNumber { get; set; }
        public DateTime? warrantyExpiresOn { get; set; }
        public string yearModel { get; set; }
        public bool? status { get; set; }
        public int? tankCapacity { get; set; }
        public string customerCode { get; set; }
        public string vehicleMakeDescription { get; set; }
        public string modelDescription { get; set; }
    }
}

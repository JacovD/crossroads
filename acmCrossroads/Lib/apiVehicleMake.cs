﻿using acmCrossroads.acmCrossroads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using PX.Data;

namespace acmCrossroads.Lib
{
    class apiVehicleMake
    {
        /*
        "countryId": 0,
        "vehicleMakeCode": "string",
        "vehicleMakeDescription": "string",
        "isActive": true
        */

        public static IRestResponse postVehicleMake(string authToken, dvVehicleMakeMaint Base, dvVehicleMake row, int CompanyID)
        {
            dvEsb dv = abitGenix.GetESBLogin(Base);
            var client = new RestClient(dv.EsbBaseUrl + "CrdEsb/api/Acumatica/VehicleMakes");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Token", authToken);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(clsVehicleMakes(Base, row, CompanyID));
            IRestResponse response = client.Execute(request);
            return response;
        }

        private static List<clsVehicleMake> clsVehicleMakes(dvVehicleMakeMaint Base, dvVehicleMake row, int CompanyID)
        {
            List<clsVehicleMake> list = new List<clsVehicleMake>();
            if (row != null)
            {
                clsVehicleMake item = new clsVehicleMake();
                item.CountryID = abitGenix.getCountryID(Base, Base.Accessinfo.CompanyName, CompanyID);
                item.vehicleMakeCode = row.VehicleMakeCode;
                item.vehicleMakeDescription = row.VehicleMakeDescription;
                item.isActive = row.Active ?? false;
                list.Add(item);
            }
            return list;
        }
    }


    public class clsVehicleMake
    {
        public int? CountryID { get; set; }
        public string vehicleMakeCode { get; set; }
        public string vehicleMakeDescription { get; set; }
        public bool? isActive { get; set; }
    }

}

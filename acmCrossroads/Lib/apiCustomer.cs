﻿using PX.Objects.AR;
using PX.Objects.AP;
using PX.Objects.CA;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.TX;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PX.Data;
using acmCrossroads.acmCrossroads;

namespace acmCrossroads.Lib
{
    class apiCustomer
    {
        public static IRestResponse postCustomer(string authToken, CustomerMaint Base, Customer row, int CompanyID)
        {
            dvEsb dv = abitGenix.GetESBLogin(Base);
            var client = new RestClient(dv.EsbBaseUrl + "CrdEsb/api/Acumatica/Customer");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Token", authToken);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(getPostBody(Base, row,CompanyID));
            IRestResponse response = client.Execute(request);
            return response;
        }

        public static List<clsCustomer> getPostBody(CustomerMaint Base, Customer row, int CompanyID)
        {

            List<clsCustomer> list = new List<clsCustomer>();
            if (row != null)
            {
                Address phyAddress = PXSelect<Address, Where<Address.addressID, Equal<Required<Address.addressID>>>>.SelectSingleBound(Base, null, row.DefAddressID);
                Address postAddress = PXSelect<Address, Where<Address.addressID, Equal<Required<Address.addressID>>>>.SelectSingleBound(Base, null, row.DefBillAddressID);
                Contact contactDetail = PXSelect<Contact, Where<Contact.contactID, Equal<Required<Contact.contactID>>>>.SelectSingleBound(Base, null, row.DefContactID);
                Location locationDetail = PXSelect<Location, Where<Location.locationID, Equal<Required<Location.locationID>>>>.SelectSingleBound(Base, null, row.DefLocationID);
                TaxZone taxZone = PXSelect<TaxZone, Where<TaxZone.taxZoneID, Equal<Required<TaxZone.taxZoneID>>>>.SelectSingleBound(Base, null, locationDetail.CTaxZoneID);
                string sTradingName = "";
                CSAnswers answers = PXSelect<CSAnswers, Where<CSAnswers.refNoteID, Equal<Required<CSAnswers.refNoteID>>, And<CSAnswers.attributeID, Equal<Required<CSAnswers.attributeID>>>>>.SelectSingleBound(Base, null, row.NoteID, "CUSKNOWNAS");
                CSAttributeDetail attributeDetail = new CSAttributeDetail();
                if (answers != null)
                {
                    sTradingName = answers.Value;
                    /*attributeDetail = PXSelect<CSAttributeDetail, Where<CSAttributeDetail.valueID, Equal<Required<CSAttributeDetail.valueID>>, And<CSAttributeDetail.attributeID, Equal<Required<CSAttributeDetail.attributeID>>>>>.SelectSingleBound(Base, null, answers.Value, "CUSKNOWNAS");
                    if (attributeDetail != null)
                    {
                        sTradingName = attributeDetail.Description ?? "";
                    }*/
                }
                string sCompanyRegistrationNumber = "";
                PaymentMethodDetail paymentMethodDetail = PXSelect<PaymentMethodDetail, Where<PaymentMethodDetail.paymentMethodID, Equal<Required<PaymentMethodDetail.paymentMethodID>>, And<PaymentMethodDetail.descr, Equal<Required<PaymentMethodDetail.descr>>>>>.SelectSingleBound(Base, null, locationDetail.PaymentMethodID, "Registration Number");
                VendorPaymentMethodDetail vendorPaymentMethodDetail = new VendorPaymentMethodDetail();
                if (paymentMethodDetail != null)

                {
                    vendorPaymentMethodDetail = PXSelect<VendorPaymentMethodDetail,
                                                Where<VendorPaymentMethodDetail.bAccountID, Equal<Required<VendorPaymentMethodDetail.locationID>>,
                                                And<VendorPaymentMethodDetail.detailID, Equal<Required<VendorPaymentMethodDetail.detailID>
                                                >>>>.SelectSingleBound(Base, null, row.DefLocationID, paymentMethodDetail.DetailID);
                    if (vendorPaymentMethodDetail != null)
                    {
                        sCompanyRegistrationNumber = vendorPaymentMethodDetail.DetailValue;
                    }
                }
                bool isActive = false;
                if (row.Status == "A")
                {
                    isActive = true;
                }

                clsCustomer item = new clsCustomer();
                item.CountryID = abitGenix.getCountryID(Base, Base.Accessinfo.CompanyName,CompanyID);
                item.CustomerCode = row.AcctCD.ToString().Trim();
                item.CustomerName = row.AcctName.Trim();
                item.PhysicalAddressLine1 = phyAddress.AddressLine1;
                item.PhysicalAddressLine2 = phyAddress.AddressLine2;
                item.PhysicalAddressLine3 = phyAddress.AddressLine3 ?? "";
                item.PhysicalAddressLine4 = phyAddress.City;
                item.PhysicalAddressAreaCode = phyAddress.PostalCode;
                item.PostalAddressLine1 = postAddress.AddressLine1;
                item.PostalAddressLine2 = postAddress.AddressLine2;
                item.PostalAddressLine3 = postAddress.AddressLine3 ?? "";
                item.PostalAddressLine4 = postAddress.City;
                item.PostalCode = postAddress.PostalCode;
                item.RegionStateProvinceCode = phyAddress.State;
                item.CountryCode = phyAddress.CountryID;
                item.CompanyRegistrationNumber = sCompanyRegistrationNumber;
                item.CompanyVatRegistrationNumber = locationDetail.TaxRegistrationID;
                item.CompanyTelephoneNumber = contactDetail.Phone1;
                item.CompanyFaxNumber = contactDetail.Fax;
                item.CompanyEmail = contactDetail.EMail;
                item.CompanyCreditLimit = row.CreditLimit ?? 0;
                if (item.CompanyCreditLimit <= 0)
                {
                    item.CompanyCreditAvailable = 0;
                }
                else
                {
                    if (Base.CustomerBalance.SelectSingle() == null)
                    {
                        item.CompanyCreditAvailable = 0;
                    }
                    else
                    {
                        item.CompanyCreditAvailable = Base.CustomerBalance.SelectSingle().RemainingCreditLimit;
                    }
                }
                item.CompanyGLCode = abitGenix.GetAccountCD(Base, Base.DefLocation.Current.CSalesAcctID);
                item.CompanyDefaultTaxCode = taxZone.DfltTaxCategoryID;
                item.CompanyDefaultCurrency = Base.CurrentCustomer.Current.CuryID;
                item.TradingName = sTradingName ?? contactDetail.FullName ?? "";
                item.IsActive = isActive;
                list.Add(item);
            }
            return list;
        }

    }

    public class clsCustomer
    {
        public int? CountryID { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string PhysicalAddressLine1 { get; set; }
        public string PhysicalAddressLine2 { get; set; }
        public string PhysicalAddressLine3 { get; set; }
        public string PhysicalAddressLine4 { get; set; }
        public string PhysicalAddressAreaCode { get; set; }
        public string PostalAddressLine1 { get; set; }
        public string PostalAddressLine2 { get; set; }
        public string PostalAddressLine3 { get; set; }
        public string PostalAddressLine4 { get; set; }
        public string PostalCode { get; set; }
        public string RegionStateProvinceCode { get; set; }
        public string CountryCode { get; set; }
        public string CompanyRegistrationNumber { get; set; }
        public string CompanyVatRegistrationNumber { get; set; }
        public string CompanyTelephoneNumber { get; set; }
        public string CompanyFaxNumber { get; set; }
        public string CompanyEmail { get; set; }
        public decimal? CompanyCreditAvailable { get; set; }
        public decimal? CompanyCreditLimit { get; set; }
        public string CompanyGLCode { get; set; }
        public string CompanyDefaultTaxCode { get; set; }
        public string CompanyDefaultCurrency { get; set; }
        public string TradingName { get; set; }
        public bool IsActive { get; set; }
    }
}

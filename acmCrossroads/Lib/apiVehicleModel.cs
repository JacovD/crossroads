﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using acmCrossroads.acmCrossroads;
using RestSharp;
using PX.Data;

namespace acmCrossroads.Lib
{
    class apiVehicleModel
    {
        /*
        "countryId": 0,
        "vehicleMakeCode": "string",
        "vehicleModelCode": "string",
        "vehicleModelDescription": "string",
        "vehicleCategory": "string",
        "vehicleModelVariant": "string",
        "vehicleMakeId": 0,
        "modelPrice": 0,
        "modelLicenseGroup": "string",
        "engineCapacity": 0,
        "fuelTankCapacity": 0,
        "unladenWeight": 0,
        "modelTareWeight": 0,
        "modelMaximumRevs": 0,
        "modelFuelConsumption": 0,
        "modelFuelType": "string",
        "cO2Emissions": 0,
        "isActive": true
        */

        public static IRestResponse postVehicleModel(string authToken, dvVehicleModelEntry Base, dvVehicleModel row, int CompanyID)
        {
            dvEsb dv = abitGenix.GetESBLogin(Base);
            var client = new RestClient(dv.EsbBaseUrl + "CrdEsb/api/Acumatica/VehicleModels");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Token", authToken);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(clsVehicleModels(Base, row, CompanyID));
            IRestResponse response = client.Execute(request);
            return response;
        }

        private static List<clsVehicleModel> clsVehicleModels(dvVehicleModelEntry Base, dvVehicleModel row, int CompanyID)
        {
            List<clsVehicleModel> list = new List<clsVehicleModel>();
            if (row != null)
            {
                clsVehicleModel item = new clsVehicleModel();
                item.CountryID = abitGenix.getCountryID(Base, Base.Accessinfo.CompanyName, CompanyID);
                item.vehicleMakeCode = row.VehicleMake;
                item.vehicleModelCode = row.ModelCode;
                item.vehicleModelDescription = row.ModelDescription;
                item.vehicleCategory = row.VehicleCategory;
                item.vehicleModelVariant = row.ModelVariant;
                item.vehicleMakeId = row.VehicleModelID;
                item.modelPrice = row.ModelPrice;
                item.modelLicenseGroup = row.ModelLicenseGroup;
                item.engineCapacity = row.EngineCapacity;
                item.fuelTankCapacity = row.FuelTankCapacity;
                item.unladenWeight = row.UnladenWeight;
                item.modelTareWeight = row.ModelTareWeight;
                item.modelMaximumRevs = row.ModelMaximumRevs;
                item.modelFuelConsumption = row.ModelFuelConsumption;
                item.modelFuelType = row.ModelFuelType;
                item.cO2Emissions = row.CO2Emissions;
                item.isActive = true;
                list.Add(item);
            }
            return list;
        }

    }

    public class clsVehicleModel
    {
        public int? CountryID { get; set; }
        public string vehicleMakeCode { get; set; }
        public string vehicleModelCode { get; set; }
        public string vehicleModelDescription { get; set; }
        public string vehicleCategory { get; set; }
        public string vehicleModelVariant { get; set; }
        public decimal? vehicleMakeId { get; set; }
        public decimal? modelPrice { get; set; }
        public string modelLicenseGroup { get; set; }
        public decimal? engineCapacity { get; set; }
        public decimal? fuelTankCapacity { get; set; }
        public decimal? unladenWeight { get; set; }
        public decimal? modelTareWeight { get; set; }
        public decimal? modelMaximumRevs { get; set; }
        public decimal? modelFuelConsumption { get; set; }
        public string modelFuelType { get; set; }
        public decimal? cO2Emissions { get; set; }
        public bool isActive { get; set; }
    }
}

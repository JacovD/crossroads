﻿using RestSharp;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using acmCrossroads.acmCrossroads;
using PX.Data;

namespace acmCrossroads.Lib
{
    public class apiAuthentication
    {
        public static string getToken(PXGraph graph)
        {
            string sToken = "";
            try
            {
                dvEsb dv = abitGenix.GetESBLogin(graph);
                var client = new RestClient(dv.EsbBaseUrl + "CrdAuth/api/Login");
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.RequestFormat = DataFormat.Json;
                request.AddBody(new
                {
                    username = dv.EsbUsername,
                    password = dv.EsbPassword
                });
                IRestResponse<authToken> response = client.Execute<authToken>(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    sToken = response.Data.Token;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return sToken;
        }

    }

    class authToken
    {
        [DeserializeAs(Name = "token")]
        public string Token { get; set; }
        [DeserializeAs(Name = "applicationUrl")]
        public string ApplicationUrl { get; set; }
    }
}

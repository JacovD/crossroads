﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PX.Common;
using PX.Data;
using PX.Objects.CS;
using PX.Objects.CR;
using PX.Objects.CM;
using PX.Objects.GL;
using PX.Objects.GL.FinPeriods;
using PX.Objects.AP;
using PX.Objects.EP;
using PX.Objects.IN;
using PX.Objects.SO;
using PX.Objects.Common.Tools;
using PX.Objects.FA.Overrides.AssetProcess;
using PX.Objects.GL.FinPeriods.TableDefinition;
using PX.Objects;
using PX.Objects.FA;
using acmCrossroads.acmCrossroads;

namespace acmCrossroads.Lib
{
    public static class abitGenix
    {
        #region CountryID
        public static int getCountryID(PXGraph graph, string Company, int companyID)
        {
            int CountryID = 0;
            if (graph != null)
            {
                if (!string.IsNullOrEmpty(Company))
                {
                    Company = PXAccess.GetBranchCD().ToString().Trim();
                    BAccount account = PXSelect<BAccount, Where<BAccount.acctCD, Equal<Required<BAccount.acctCD>>, And<BAccount.type, Equal<Required<BAccount.type>>>>>.Select(graph, Company, BAccountType.OrganizationBranchCombinedType);
                    if (account != null)
                    {
                        if (account.BAccountID.HasValue && account.DefAddressID.HasValue)
                        {
                            Address address = PXSelect<Address, Where<Address.bAccountID, Equal<Required<Address.bAccountID>>, And<Address.addressID, Equal<Required<Address.addressID>>>>>.Select(graph, account.BAccountID, account.DefAddressID);
                            if (address != null)
                            {
                                if (!string.IsNullOrEmpty(address.CountryID))
                                {
                                    switch (address.CountryID)
                                    {
                                        case "ZA":
                                            {
                                                CountryID = 1;
                                                break;
                                            }
                                        case "NA":
                                            {
                                                CountryID = 2;
                                                break;
                                            }
                                        default:
                                            {
                                                CountryID = 0;
                                                break;
                                            }
                                    }
                                }


                            }
                        }
                    }


                    /*Fail Safe*/
                    if (CountryID == 0)
                    {
                        switch (companyID)
                        {
                            case 2: // RSA(CRD)
                                {
                                    CountryID = 1;
                                    break;
                                }
                            case 3: // NAM
                                {
                                    CountryID = 2;
                                    break;
                                }
                            default:
                                {
                                    CountryID = 0;
                                    break;
                                }
                        }
                    }
                }
            }
            return CountryID;
        }
        #endregion

        #region Accounts
        public static string GetAccountCD(PXGraph graph, int? AccountID)
        {
            string accountCD = "";
            if (AccountID.HasValue)
            {
                Account account = PXSelect<Account, Where<Account.accountID, Equal<Required<Account.accountID>>>>.SelectSingleBound(graph, null, AccountID);
                if (account != null)
                {
                    if (!string.IsNullOrEmpty(account.AccountCD))
                    {
                        accountCD = account.AccountCD;
                    }
                }
            }
            return accountCD;
        }

        public static string GetSubAccountCD(PXGraph graph, int? SubAccountID)
        {
            string accountCD = "";
            if (SubAccountID.HasValue)
            {
                Sub account = PXSelect<Sub, Where<Sub.subID, Equal<Required<Sub.subCD>>>>.SelectSingleBound(graph, null, SubAccountID);
                if (account != null)
                {
                    if (!string.IsNullOrEmpty(account.SubCD))
                    {
                        accountCD = account.SubCD;
                    }
                }
            }
            return accountCD;
        }
        #endregion

        #region Fixed Assets
        public static string GetFixedAssetClassCD(PXGraph graph, int? FixedAssetClassID)
        {
            string CD = "";
            if (FixedAssetClassID.HasValue)
            {
                FixedAsset Data = PXSelect<FixedAsset, Where<FixedAsset.assetID, Equal<Required<FixedAsset.assetID>>, And<FixedAsset.recordType, Equal<Required<FixedAsset.recordType>>>>>.SelectSingleBound(graph, null, FixedAssetClassID, "C");
                if (Data != null)
                {
                    if (!string.IsNullOrEmpty(Data.AssetCD))
                    {
                        CD = Data.AssetCD;
                    }
                }
            }
            return CD;
        }

        public static string GetFixedAssetLessor(PXGraph graph, int? LessorID)
        {
            string CD = "";
            if (LessorID.HasValue)
            {
                Vendor Data = PXSelect<Vendor, Where<Vendor.bAccountID, Equal<Required<Vendor.bAccountID>>>>.SelectSingleBound(graph, null, LessorID);
                if (Data != null)
                {
                    if (!string.IsNullOrEmpty(Data.AcctCD))
                    {
                        CD = Data.AcctCD;
                    }
                }
            }
            return CD;
        }



        #endregion

        #region ESB
        public static dvEsb GetESBLogin(PXGraph graph)
        {
            dvEsb dv = dv = PXSelect<dvEsb>.SelectSingleBound(graph, null);
            return dv;
        }
        #endregion

        #region LastSyncDate

        public static void setLastSyncDate(PXGraph graph, string syncLocation, string refNbr)
        {
            PXDatabase.Insert<dvESBsync>(new PXDataFieldAssign<dvESBsync.syncLocation>(syncLocation),
                                         new PXDataFieldAssign<dvESBsync.refNbr>(refNbr),
                                         new PXDataFieldAssign<dvESBsync.dateSynced>(DateTime.Now.ToString("dd MM yyyy hh:mm:ss")));
        }

        public static string getLastSyncDate(PXGraph graph, string syncLocation, string refNbr)
        {
            string syncDate = "";
            if(!string.IsNullOrEmpty(syncLocation) && !string.IsNullOrEmpty(refNbr))
            {
                dvESBsync row = PXSelect<dvESBsync, 
                    Where<dvESBsync.syncLocation, Equal<Required<dvESBsync.syncLocation>>, 
                    And<dvESBsync.refNbr, Equal<Required<dvESBsync.refNbr>>>>, 
                    OrderBy<Desc<dvESBsync.DvESBsyncID>>>.SelectSingleBound(graph, null, syncLocation, refNbr);
                if(row != null)
                {
                    if (!string.IsNullOrEmpty(row.DateSynced))
                    {
                        syncDate = row.DateSynced;
                    }
                }
            }
            return syncDate;
        }
        #endregion
    }
}

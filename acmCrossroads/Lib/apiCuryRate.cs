﻿using PX.Objects.CM;
using RestSharp;
using System;
using System.Collections.Generic;
using PX.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using acmCrossroads.acmCrossroads;

namespace acmCrossroads.Lib
{
    class apiCuryRate
    {
        public static IRestResponse postCuryRate(string authToken, CuryRateMaint Base, CuryRateFilter row, int CompanyID)
        {
            dvEsb dv = abitGenix.GetESBLogin(Base);
            //var client = new RestClient(dv.EsbBaseUrl + "CrdEsb/api/Acumatica/FixedAsset");
            var client = new RestClient(dv.EsbBaseUrl + "CrdEsb/api/Acumatica/ExchangeRate");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Token", authToken);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(clsCuryRates(Base, row, CompanyID));
            IRestResponse response = client.Execute(request);
            return response;
        }

        private static List<clsCuryRate> clsCuryRates(CuryRateMaint Base, CuryRateFilter row, int CompanyID)
        {
            List<clsCuryRate> list = new List<clsCuryRate>();
            PXResultset<CurrencyRate> currencyRates = PXSelect<CurrencyRate, Where<CurrencyRate.toCuryID, Equal<Required<CurrencyRate.toCuryID>>, And<CurrencyRate.curyEffDate, Equal<Required<CurrencyRate.curyEffDate>>>>>.Select(Base, row.ToCurrency, DateTime.Today);
            if (currencyRates != null)
            {
                foreach (CurrencyRate item in currencyRates)
                {
                    clsCuryRate rate = new clsCuryRate();
                    rate.CountryID = abitGenix.getCountryID(Base, Base.Accessinfo.CompanyName, CompanyID);
                    rate.toCurrency = row.ToCurrency;
                    rate.fromCurrency = item.FromCuryID;
                    rate.currencyRateType = item.CuryRateType;
                    rate.currencyEffectiveDate = item.CuryEffDate;
                    rate.currencyRate = item.CuryRate;
                    rate.currencyMultDiv = item.CuryMultDiv;
                    rate.currencyRateReciprocal = item.RateReciprocal;
                    list.Add(rate);
                }
            }
            return list;
        }
    }

    public class clsCuryRate
    {
        public int? CountryID { get; set; }
        public string toCurrency { get; set; }
        public string fromCurrency { get; set; }
        public string currencyRateType { get; set; }
        public DateTime? currencyEffectiveDate { get; set; }
        public decimal? currencyRate { get; set; }
        public string currencyMultDiv { get; set; }
        public decimal? currencyRateReciprocal { get; set; }
    }


}
